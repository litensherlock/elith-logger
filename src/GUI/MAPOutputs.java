package GUI;

import dataOutput.ERout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Jakob Lövhall on 2014-03-08.
 */
public class MAPOutputs extends JPanel {
    private static final int MY_WIDTH = 100;
    private static final int MY_HEIGHT = 200;

    private JTextArea textArea;

    public MAPOutputs(OutputStream stream) {

        setSize(new Dimension(MY_WIDTH, MY_HEIGHT));

        ERout.getInstance().openCOMOutputStream(stream);

        textArea = new JTextArea("0",1,5);
        textArea.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    mapEnter();
                } else if (e.getKeyCode() == KeyEvent.VK_UP){
                    mapUp(1);
                } else if (e.getKeyCode() == KeyEvent.VK_DOWN){
                    mapDown(1);
                } else if (e.getKeyCode() == KeyEvent.VK_RIGHT){
                    mapUp(10);
                } else if (e.getKeyCode() == KeyEvent.VK_LEFT){
                    mapDown(10);
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        add(textArea);

        setVisible(true);
    }

    private void mapEnter() {
        Short aShort = tryPars();
        if (aShort == null){
            return;
        }

        if (aShort > 100)
            aShort = 100;
        else if (aShort < 0)
            aShort = 0;

        try {
            ERout.getInstance().sendPackage(aShort);
        } catch (IOException e1) {
            System.out.println(e1);
        }

        textArea.setText(aShort.toString());
    }

    private void mapUp(int n) {
        Short aShort = tryPars();
        if (aShort == null){
            return;
        }

        if (aShort + n > 100)
            aShort = 100;
        else
            aShort = (short)(aShort + n);

        try {
            ERout.getInstance().sendPackage(aShort);
        } catch (IOException e1) {
            System.out.println(e1);
        }

        textArea.setText(aShort.toString());
    }

    private void mapDown(int n) {
        Short aShort = tryPars();
        if (aShort == null){
            return;
        }

        if (aShort - n < 0)
            aShort = 0;
        else
            aShort = (short)(aShort - n);

        try {
            ERout.getInstance().sendPackage(aShort);
        } catch (IOException e1) {
            System.out.println(e1);
        }

        textArea.setText(aShort.toString());
    }

    private Short tryPars(){
        String s = textArea.getText();

        try {
            return Short.valueOf(s);
        } catch (NumberFormatException ignored){
        }

        return null;
    }
}
