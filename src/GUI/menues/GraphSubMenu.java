package GUI.menues;

import GUI.charting.ChartHandler;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-03
 * Time: 16:31
 */
class GraphSubMenu extends JMenu {
    private ValuesMenu valuesMenu;

    GraphSubMenu(String s, final ChartHandler chartHandler) {
        super(s);

        valuesMenu = new ValuesMenu(chartHandler);

        add(valuesMenu);
    }
}
