package GUI.menues;

import common.Config;
import common.Data;
import common.Sensor;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-03
 * Time: 16:43
 */
class FilterMenu extends JMenu{
    FilterMenu() {
        super("Filters");

        Sensor[] protocol = Data.getInstance().getProtocol();
        for (Sensor sensor : protocol) {
            JCheckBoxMenuItem value = new JCheckBoxMenuItem(sensor.name());
            value.setState(false);

            value.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Sensor selectedSensor = Sensor.valueOf(e.getActionCommand());

                    JCheckBoxMenuItem box = (JCheckBoxMenuItem) e.getSource();

                    if (box.isSelected()) {
                        Data.getInstance().getChartbufferHandler().setFilter(selectedSensor, Config.getInstance().getFilterFactory().createFilter(selectedSensor));
                    } else
                        Data.getInstance().getChartbufferHandler().removeFilters(selectedSensor);

                }
            });

            add(value);
        }
    }
}