package GUI.menues;

import GUI.charting.ChartHandler;
import common.Data;
import common.Sensor;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-03
 * Time: 16:43
 */
class ValuesMenu extends JMenu{
    ValuesMenu(final ChartHandler chartHandler) {
        super("Values");

        Sensor[] protocol = Data.getInstance().getProtocol();
        for (Sensor sensor : protocol) {
            JCheckBoxMenuItem value = new JCheckBoxMenuItem(sensor.name());

            value.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Sensor selectedSensor = Sensor.valueOf(e.getActionCommand());

                    JCheckBoxMenuItem box = (JCheckBoxMenuItem) e.getSource();

                    if (box.isSelected()) {
                        chartHandler.addLine(selectedSensor);
                    } else
                        chartHandler.removeLine(selectedSensor);

                }
            });

            add(value);
        }
    }
}