package GUI.menues;

import GUI.ERLoggerFrame;
import common.Config;
import common.Sensor;
import common.SerialPortHandler;

import javax.swing.*;
import javax.swing.plaf.DimensionUIResource;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-02-24
 * Time: 20:49
 */
public class MenuBar extends JMenuBar implements ProtocolChangeListener{
    private static final int PORTS_WIDTH = 90;
    private static final int PORTS_HEIGHT = 30;
    private GraphMenu graphMenu;
    private ERLoggerFrame erLoggerFrame;
    private FilterMenu filterMenu;
    private AdditionalWindows additionalWindows;

    public MenuBar(ERLoggerFrame erLoggerFrame){
        this.erLoggerFrame = erLoggerFrame;

        //Options for the JComboBox
        String[] portNames = SerialPortHandler.getInstance().getSerialPortNames();

        final JComboBox<String> ports = new JComboBox<>(portNames);

        ports.setMaximumSize(new DimensionUIResource(PORTS_WIDTH, PORTS_HEIGHT));

        final FileMenu fileMenu = new FileMenu(ports);
        add(fileMenu);

        add(new LoggingMenu());

        add(ports);


        graphMenu = new GraphMenu(erLoggerFrame);
        add(graphMenu);

        filterMenu = new FilterMenu();
        add(filterMenu);

        additionalWindows = new AdditionalWindows(ports, fileMenu.getFilebutton());
        add(additionalWindows);

        Config.getInstance().addProtocolChangeListener(this);
    }

    @Override
    public void protocolChange(Sensor[] protocol) {
        remove(additionalWindows);
        remove(graphMenu);
        remove(filterMenu);

        graphMenu.resetCharts();
        graphMenu = new GraphMenu(erLoggerFrame);
        filterMenu = new FilterMenu();

        add(graphMenu);
        add(filterMenu);
        add(additionalWindows);
    }
}
