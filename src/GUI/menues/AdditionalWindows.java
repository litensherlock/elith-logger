package GUI.menues;

import GUI.DotDiagram;
import GUI.MAPFrame;
import common.SerialPortHandler;
import dataInput.ERin;
import dataInput.SerialLoader;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.OutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-05-17
 */

/**
 * menu for those functions that open a new window
 */
class AdditionalWindows extends JMenu{
    private MAPFrame mapFrame;

    AdditionalWindows(final JComboBox<String> ports, final JMenuItem fileButton) {
        super("Extra Windows");

        mapFrame = null;
        final JMenuItem MAPControlButton = new JMenuItem("MAP controls"); // For the mapping controlls.
        final JMenuItem dotDiagramButton = new JMenuItem("Dot diagram");

        MAPControlButton.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, InputEvent.CTRL_MASK));
        MAPControlButton.setMnemonic('M');
        MAPControlButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                connect(ports, fileButton);

                if (mapFrame != null) {
                    mapFrame.setVisible(true);
                    return;
                }

                SerialPortHandler ers = SerialPortHandler.getInstance();

                if (ers.isPortOpen()) {
                    OutputStream stream = ers.getOutputStream();

                    mapFrame = new MAPFrame(stream);
                }
            }
        });

        dotDiagramButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DotDiagram dotDiagram = new DotDiagram();

                JFrame frame2 = new JFrame("dot diagram");
                frame2.setAutoRequestFocus(false);
                frame2.add(dotDiagram);
                frame2.setSize(dotDiagram.getSize());
                frame2.setVisible(true);
            }
        });

        //adds all the buttons to the bar
        add(MAPControlButton);
        add(dotDiagramButton);
    }

    //NOTE this is a duplicate from the function in FileMenu

    /**
     * this is a duplicate from the function in FileMenu. it is used for the MAP controls
     * @param ports
     * @param fileButton
     */
    private void connect(JComboBox<String> ports, JMenuItem fileButton) {
        SerialPortHandler ers = SerialPortHandler.getInstance();
        String s = (String) ports.getSelectedItem();

        if (!ers.isPortOpen()){
            System.out.println("trying to connect to " + s);
            ers.openSerialPort(s);
        }

        if (ers.isPortOpen()){
            SerialLoader loader = new SerialLoader(ers.getInputStream());
            loader.initWireless();
            ERin.getInstance().setLoader(loader);
            fileButton.setEnabled(false);
            ports.setEnabled(false);
        }
    }
}
