package GUI.menues;

import dataOutput.ERout;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-02-24
 * Time: 21:57
 */
class LoggingMenu extends JMenu {

    LoggingMenu() {
        super("Logging");

        final JCheckBoxMenuItem binLogging = new JCheckBoxMenuItem("bin logging");
        final JCheckBoxMenuItem textLogging = new JCheckBoxMenuItem("text logging");

        //starts the bin logging to file
        binLogging.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ERout ERout = dataOutput.ERout.getInstance();// won't compile if this is done via a import
                if (ERout.isBinLogging()) {
                    ERout.stopBinLogging();
                } else {
                    ERout.startBinLogging();
                }

            }
        });

        //starts the text logging to file
        textLogging.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ERout ERout = dataOutput.ERout.getInstance();// won't compile if this is done via a import
                if (ERout.isTextLogging()) {
                    ERout.stopTextLogging();
                } else {
                    ERout.startTextLogging();
                }
            }
        });

        add(binLogging);
        add(textLogging);
    }
}
