package GUI.menues;

import GUI.ERLoggerFrame;
import GUI.charting.ChartHandler;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-02-24
 * Time: 21:57
 */
class GraphMenu extends JMenu {

    private ChartHandler[] handlers;

    GraphMenu(ERLoggerFrame erLoggerFrame) {
        super("Graphs");

        int numberOfGraphs = 3;

        handlers = new ChartHandler[3];

        handlers [0] = new ChartHandler(erLoggerFrame.getGraph1());
        handlers [1] = new ChartHandler(erLoggerFrame.getGraph2());
        handlers [2] = new ChartHandler(erLoggerFrame.getGraph3());

        for (int i = 0; i < numberOfGraphs; i++) {
            GraphSubMenu gsm = new GraphSubMenu("Graph " + i, handlers[i]);

            add(gsm);
        }
    }

    public void resetCharts(){
        for (ChartHandler handler : handlers) {
            handler.resetChart();
        }
    }
}
