package GUI.menues;

import GUI.ERFileChooser;
import common.Config;
import common.SerialPortHandler;
import dataInput.ERin;
import dataInput.SerialLoader;
import dataOutput.ERout;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-02-28
 * Time: 17:07
 */
class FileMenu extends JMenu {
    private final JMenuItem fileButton;

    FileMenu(final JComboBox<String> ports) {
        super("File");

        final JMenuItem connectButton = new JMenuItem("Connect");
        final JMenuItem disConnectButton = new JMenuItem("Disconnect");
        fileButton = new JMenuItem("Open file");
        final JMenuItem offsetButton = new JMenuItem("Create acc offset");
        final JMenuItem quitButton = new JMenuItem("Quit");

        connectButton.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));
        connectButton.setMnemonic('C');
        // switching type. tries to connect or disconnect from a port
        connectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                connect(ports, fileButton);
            }
        });

        disConnectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SerialPortHandler ers = SerialPortHandler.getInstance();

                ers.closeSerialPort();
                ERin.getInstance().setLoader(null);
                ERout.getInstance().closeCOMOutputStream();
                fileButton.setEnabled(true);

                if (!ers.isPortOpen())
                    ports.setEnabled(true);
            }

        });


        fileButton.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
        fileButton.setMnemonic('O');
        //opens a file dialog and gives valid suffixes a loader
        fileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                //if you enter fileChooser and then exit by cancel it will return a null pointer
                File file = new ERFileChooser().getFile();

                if(file != null){
                    ERin.getInstance().setInput(file);
                }
            }
        });

        //creates a new offset file
        offsetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Config.getInstance().setOffsetToCurrentValues();
            }
        });

        quitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ERout.getInstance().stopBinLogging();
                ERout.getInstance().stopTextLogging();
                System.exit(0);
            }
        });


        //adds all the buttons to the bar
        add(connectButton);
        add(disConnectButton);
        add(fileButton);
        add(offsetButton);
        add(quitButton);
    }

    //NOTE this is a duplicate from the function in AdditionalWindows
    private void connect(JComboBox<String> ports, JMenuItem fileButton) {
        SerialPortHandler ers = SerialPortHandler.getInstance();
        String s = (String) ports.getSelectedItem();

        if (!ers.isPortOpen()){
            System.out.println("trying to connect to " + s);
            ers.openSerialPort(s);
        }

        if (ers.isPortOpen()){
            SerialLoader loader = new SerialLoader(ers.getInputStream());
            loader.initWireless();
            ERin.getInstance().setLoader(loader);
            fileButton.setEnabled(false);
            ports.setEnabled(false);
        }
    }

    public JMenuItem getFilebutton(){
        return fileButton;
    }
}
