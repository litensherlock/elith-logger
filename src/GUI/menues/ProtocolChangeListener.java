package GUI.menues;

import common.Sensor;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-22
 */
public interface ProtocolChangeListener {
    public void protocolChange(Sensor[] protocol);
}
