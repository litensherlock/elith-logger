package GUI;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob lövhall
 * Date: 2013-11-02
 * Time: 12:31
 */
public class ERFileChooser extends JFileChooser {

    private FileNameExtensionFilter filter;

    public ERFileChooser() {
        filter= new FileNameExtensionFilter(
                "txt & bin", "txt","bin");

    }

    public File getFile(){

        setFileFilter(filter);
        int returnVal = showOpenDialog(getParent());
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            System.out.println("opening: " +
                    getSelectedFile().getName());
            return getSelectedFile();
        }
        return null;
    }
}
