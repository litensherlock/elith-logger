package GUI;

import javax.swing.*;
import java.io.OutputStream;

/**
 * Created by Jakob Lövhall on 2014-03-08.
 */
public class MAPFrame extends JFrame {

    public MAPFrame(OutputStream stream) {

        super("map stuff");

        MAPOutputs mapOutputs = new MAPOutputs(stream);

        add(mapOutputs);

        setSize(mapOutputs.getSize());

        setVisible(true);
    }
}
