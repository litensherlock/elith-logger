package GUI; /**
 * Created with IntelliJ IDEA.
 * User: Jakob lövhall
 * Date: 2013-10-12
 * Time: 20:57
 */

import GUI.charting.Chart;
import GUI.menues.MenuBar;
import common.Data;
import common.DataListener;
import common.ERDataField;
import dataInput.ERin;

import javax.swing.*;
import java.awt.*;

/**
 * Main window of the logger. Used for all interaction with the program.
 */
public class ERLoggerFrame implements DataListener {

    private static final int WIDTH = 1030;
    private static final int HIGHT = 750;
    private static final int NUMBER_OF_DECIMALS = 6;


    private Chart graph1;
    private Chart graph2;
    private Chart graph3;

    private JTextArea _textArea;

    public ERLoggerFrame()
    {

        final JFrame guiFrame = new JFrame();

        graph1 = new Chart(Data.getBufferSize());
        graph2 = new Chart(Data.getBufferSize());
        graph3 = new Chart(Data.getBufferSize());

        //make sure the program exits when the frame closes
        guiFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        guiFrame.setTitle("Elith Racing logger");
        guiFrame.setSize(WIDTH, HIGHT);

        //This will center the JFrame in the middle of the screen
        guiFrame.setLocationRelativeTo(null);

        _textArea =new JTextArea();
        _textArea.setFocusable(false);

        JPanel graphPanel=new JPanel();
        JPanel southGraphPanel=new JPanel();

        graphPanel.add(graph1, BorderLayout.EAST);
        graphPanel.add(graph2, BorderLayout.WEST);

        southGraphPanel.add(graph3, BorderLayout.WEST);
        southGraphPanel.add(_textArea, BorderLayout.EAST);

        MenuBar menuBar = new MenuBar(this);
        guiFrame.setJMenuBar(menuBar);

        guiFrame.add(southGraphPanel,BorderLayout.SOUTH);

        guiFrame.add(graphPanel,BorderLayout.WEST);

        //make sure the JFrame is visible
        guiFrame.setVisible(true);

        //subscribe to data changes
        ERin.getInstance().addListener(this);
    }

    public Chart getGraph1() {
        return graph1;
    }

    public Chart getGraph2() {
        return graph2;
    }

    public Chart getGraph3() {
        return graph3;
    }

    public void dataUpdated(Data data){

        StringBuilder stringBuilder=new StringBuilder();

        for (ERDataField dataField : data.getSensors()){
            stringBuilder.append(dataField.getSensor());
            stringBuilder.append(" ");

            String v = String.valueOf(dataField.getValue());

            if (v.length() > NUMBER_OF_DECIMALS)
                v = v.substring(0, NUMBER_OF_DECIMALS);


            stringBuilder.append(v);
            stringBuilder.append("\n");
        }

        //remove last end line
        stringBuilder.deleteCharAt(stringBuilder.lastIndexOf("\n"));

        _textArea.setText(stringBuilder.toString());
    }
}