package GUI;

import common.Data;
import common.DataListener;
import common.Sensor;
import dataInput.ERin;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob lövhall
 * Date: 2013-12-27
 * Time: 01:24
 */
public class DotDiagram extends JPanel implements DataListener{

    private static final int BYTE_MASK = 0xff;
    private static final int BYTE_LENGTH = 8;
    private static final int MY_WIDTH = 200;
    private static final int MY_HEIGHT = 200;

    private int[][] colorMap;

    public DotDiagram() {
        setSize(MY_WIDTH, MY_HEIGHT);
        setVisible(true);
        colorMap = new int[MY_HEIGHT][MY_WIDTH];
        ERin.getInstance().addListener(this);
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);

        float xf,yf;

        int x,y;

        if (Data.getInstance().getChartbuffer(Sensor.AccX) == null){
            xf = 0;
        } else {
            xf = Data.getInstance().getChartbuffer(Sensor.AccX).getLast().y;
        }

        if (Data.getInstance().getChartbuffer(Sensor.AccY) == null){
            yf = 0;
        } else {
            yf = Data.getInstance().getChartbuffer(Sensor.AccY).getLast().y;
        }

        //center x,y
        x = colorMap[0].length/2;
        y = colorMap.length/2;

        //a guess on how to move the dot
        x -= (int) (xf * colorMap[0].length / 4);
        y -= (int) (yf * colorMap.length / 4);

        //paint a track for where the dot have been and paint the current position white to make it visible.
        for (int i = 0; i < colorMap.length; i++) {
            for (int j = 0; j < colorMap[i].length; j++) {
                if (i == x && y == j){
                    colorMap[i][j] = Color.red.getRGB();
                    g.setColor(Color.white);
                }
                else if (checkHorizon(i,j)){
                    g.setColor(Color.blue);
                }
                else{
                    g.setColor(new Color(colorMap[i][j]));
                }

                g.drawRect(i,j,1,1);
            }
        }

        fade();
    }

    /**
     * fading is done by decreessing the intensity of all the colors by 1 untill they reach 0.
     */
    private void fade() {

        for (int i = 0; i < colorMap.length; i++) {
            for (int j = 0; j < colorMap[i].length; j++) {
                int red, green, blue;

                //extract the colors
                red = colorMap[i][j] & (BYTE_MASK << BYTE_LENGTH * 2);
                green = colorMap[i][j] & (BYTE_MASK << BYTE_LENGTH);
                blue = colorMap[i][j] & BYTE_MASK;

                //fade the colors
                red -= red > 0 ? (1 << BYTE_LENGTH * 2) : 0;
                green -= green > 0 ? (1 << BYTE_LENGTH) : 0;
                blue -= blue > 0 ? 1 : 0;

                colorMap[i][j] = red + green + blue;
            }
        }
    }

    //check for a circle around the center. the circle is at 1,5g
    private boolean checkHorizon(int y, int x){
        int margin = colorMap.length/2;

        //change this to move the horizon. 4 = 2g, 8 = 1g
        int divider = 8;

        //center x and y
        x -= colorMap[0].length/2;
        y -= colorMap.length/2;

        //check if they are on the circles edge +- margin
        return (x*x + y*y <= (colorMap.length*colorMap.length)/divider + margin) &&
                (x*x + y*y >= (colorMap.length*colorMap.length)/divider - margin);
    }

    @Override
    public void dataUpdated(Data data) {
        repaint();
    }
}
