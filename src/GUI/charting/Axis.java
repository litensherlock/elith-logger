package GUI.charting;

/**
 * Constants for the two axis
 */
public final class Axis {
    public static final int X = 0; // these are the accepted names of the two normal axis in a chart.
    public static final int Y = 1;

    private Axis() {
    }
}
