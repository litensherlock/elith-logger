package GUI.charting;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

/**
 * Main class for the graphs. Rendering and configuration is done via this.
 */
public class Graph {
    private static final float SPLITT = 2.0f;
    private final GraphConfig config;
    private Map<String, Iterable<PointF>> sources = null;
    private BufferedImage background = null;

    public Graph() {
        config = new GraphConfig(2);
    }

    public GraphConfig.Instance configure() {
        background = null;
        return config.configure();
    }

    public void setDataSources(Map<String, Iterable<PointF>> sources) {
        this.sources = sources;
    }

    public void update(Graphics2D g, int width, int height) {
        if (background == null) {
            background = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
            Graphics2D gx = (Graphics2D) background.getGraphics();
            gx.setRenderingHints(g.getRenderingHints());
            renderChart(gx, width, height);
        }

        g.drawImage(background, 0, 0, null);
        renderLines(g, width, height);
    }

    private void renderChart(Graphics2D g, int width, int height) {
        int m = config.marginSize();
        int tw = width - m*2, th = height - m*2;
        int gu = config.gutterSize();
        int w = tw - gu, h = th - gu;

        final float scaleX = w / (config.axis(Axis.X).max() - config.axis(Axis.X).min());
        final float scaleY = h / (config.axis(Axis.Y).max() - config.axis(Axis.Y).min());

        g.setBackground(config.backColor());
        g.clearRect(0, 0, width, height);

        g.translate(m,m);
        // Render grid

        FontMetrics fntMet = g.getFontMetrics();
        renderGrid(g, tw, gu, w, h, scaleX, scaleY);

        int sh = fntMet.getAscent();

        // Y-axis labels
        renderYAxisLabels(g, gu, h, scaleY, fntMet, sh);

        // X-axis caption
        {
            String str = config.axis(Axis.X).label();
            int sw = fntMet.stringWidth(str);
            g.drawString(str, gu + (w - sw) / 2, th - (sh / 2 + 4));
        }

        // X-axis labels
        AffineTransform trans = g.getTransform();
        renderXAxisLabel(g, th, gu, w, scaleX, fntMet, sh);

        // Y-axis caption
        {
            String str = config.axis(Axis.Y).label();
            int sw = fntMet.stringWidth(str);
            g.drawString(str, gu + (h - sw) / 2, sh + 4);
        }

        g.setTransform(trans);

        // Legend
        int tmpw = tw;
        int tmpy = th - 4;
        int boxh = fntMet.getMaxAscent();

        Map<String, LineConfig> lines = config.getLines();
        for (Map.Entry<String, LineConfig> stringLineConfigEntry : lines.entrySet()) {
            int sw = fntMet.stringWidth(stringLineConfigEntry.getKey());
            g.setColor(config.labelColor());
            g.drawString(stringLineConfigEntry.getKey(), tmpw - sw - 4, tmpy);
            g.setColor(stringLineConfigEntry.getValue().color());
            g.fillRect(tmpw - sw - 6 - boxh, tmpy - boxh, boxh, boxh);

            tmpw = tmpw - sw - 6 - boxh - 10;
        }
    }

    private void renderGrid(Graphics2D g, int tw, int gu, int w, int h, float scaleX, float scaleY) {
        float xStep = config.axis(Axis.X).gridStep() * scaleX;
        for (float lx = gu;lx < w + gu + xStep;lx += xStep) {
            // vertical
            g.setColor(config.gridColor());
            g.drawLine((int)lx, 0, (int)lx, h + 2);
        }

        float yStep = -config.axis(Axis.Y).gridStep() * scaleY;
        for (float ly = h; ly > yStep; ly += yStep) {
            // horizontal
            g.drawLine(gu - 2, (int)ly, tw, (int)ly);
        }
    }

    private void renderYAxisLabels(Graphics2D g, int gu, int h, float scaleY, FontMetrics fntMet, int sh) {
        float v = config.axis(Axis.Y).min();
        float yLableStep = -config.axis(Axis.Y).gridStep() * scaleY;
        for (float ly = h;ly > yLableStep;ly += yLableStep) {
            String str = Float.toString(v);
            g.setColor(config.labelColor());
            int sw = fntMet.stringWidth(str);
            g.drawString(str, gu - sw - 4, ly + sh / SPLITT);
            v += config.axis(Axis.Y).gridStep();
        }
    }

    private void renderXAxisLabel(Graphics2D g, int th, int gu, int w, float scaleX, FontMetrics fntMet, int sh) {
        float v;
        g.translate(0, th);
        g.rotate(-Math.PI/2);
        v = config.axis(Axis.X).min();
        float xLabelStep = config.axis(Axis.X).gridStep() * scaleX;
        for (float lx = gu;lx < w + gu + xLabelStep;lx += xLabelStep) {
            String str = Float.toString(v);
            int sw = fntMet.stringWidth(str);
            g.drawString(str, gu - sw - 4, lx + sh / SPLITT);
            v += config.axis(Axis.X).gridStep();
        }
    }

    private void renderLines(Graphics2D g, int width, int height) {
        int m = config.marginSize();
        int tw = width - m*2, th = height - m*2;
        int gu = config.gutterSize();
        int w = tw - gu, h = th - gu;
        float scaleX = w / (config.axis(Axis.X).max() - config.axis(Axis.X).min());
        float scaleY = h / (config.axis(Axis.Y).max() - config.axis(Axis.Y).min());

        HashMap<String, LineConfig> lines = config.getLines();

        g.translate(m, m);

        g.setClip(gu, 0, w, h);
        for (Map.Entry<String, Iterable<PointF>> stringIterableEntry : sources.entrySet()) {
            LineConfig line = lines.get(stringIterableEntry.getKey());

            g.setColor(line.color());
            g.setStroke(new BasicStroke(line.width()));

            float minx = config.axis(Axis.X).min();
            float miny = config.axis(Axis.Y).min();
            PointF last = null;
            for (PointF p : stringIterableEntry.getValue()) {
                if (last != null) {
                    g.drawLine(
                            (int)(gu + ((last.x - minx) * scaleX)),
                            (int)(h - ((last.y - miny) * scaleY)),
                            (int)(gu + ((p.x - minx) * scaleX)),
                            (int)(h - ((p.y - miny) * scaleY)));
                }
                last = p;

            }
        }
        g.setClip(0,0,tw,th);
    }


}
