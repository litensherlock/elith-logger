package GUI.charting;

import common.Data;
import common.Sensor;

import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-16
 */
public class DataLine implements Iterable<PointF>{

    private final String _name;
    private final LineConfig _config;

    public DataLine(String name, LineConfig config) {
        this._name = name;
        this._config = config;
    }

    public String getName() {
        return _name;
    }

    public LineConfig getConfig() {
        return _config;
    }

    public float getMax(){
        return Data.getInstance().getSensorMax(Sensor.valueOf(_name));
    }

    public float getMin(){
        return Data.getInstance().getSensorMin(Sensor.valueOf(_name));
    }

    @Override
    public Iterator<PointF> iterator() {
        return Data.getInstance().getChartbuffer(Sensor.valueOf(_name)).iterator();
    }
}


