package GUI.charting;

import java.awt.*;
import java.util.HashMap;

public class GraphConfig {
    private static final int DEFAULT_GUTTER_SIZE = 60;

    private final GraphConfig _parent = this;
    private final AxisConfig[] _axes;
    private final HashMap<String, LineConfig> _lines;
    private Color _gridColor = Color.LIGHT_GRAY;
    private Color _backColor = Color.WHITE;
    private int _gutterSize = DEFAULT_GUTTER_SIZE;
    private Color _labelColor = Color.BLACK;
    private int _marginSize = 8;

    public GraphConfig(int axisCount) {
        _lines = new HashMap<>();
        _axes = new AxisConfig[axisCount];
        for (int i = 0; i < _axes.length; i++) {
            _axes[i] = new AxisConfig();
        }
    }

    public AxisConfig[] getAxes() {
        return _axes;
    }

    public HashMap<String, LineConfig> getLines() {
        return _lines;
    }

    public Instance configure() {
        return new Instance();
    }

    public LineConfig.Instance newLine(String line) {
        return newLine(line, new LineConfig());
    }

    public LineConfig.Instance newLine(String line, LineConfig value) {
        _lines.put(line, value);
        return value.configure(this.configure());
    }

    public GraphConfig deleteLine(String line) {
        _lines.remove(line);
        return this;
    }

    public LineConfig.Instance line(String line, LineConfig value) {
        _lines.put(line, value);
        return value.configure(this.configure());
    }

    public LineConfig.Instance line(String line) {
        return _lines.get(line).configure(this.configure());
    }

    public AxisConfig.Instance axis(int axis, AxisConfig value) {
        _axes[axis] = value;
        return value.configure(this.configure());
    }

    public AxisConfig.Instance axis(int axis) {
        return _axes[axis].configure(this.configure());
    }

    public Color backColor() { return _backColor; }
    public GraphConfig backColor(Color color) {
        _backColor = color;
        return this;
    }

    public Color gridColor() { return _gridColor; }
    public GraphConfig gridColor(Color color) {
        _gridColor = color;
        return this;
    }

    public int gutterSize() { return _gutterSize; }
    public GraphConfig gutterSize(int size) {
        _gutterSize = size;
        return this;
    }

    public int marginSize() { return _marginSize; }
    public GraphConfig marginSize(int size) {
        _marginSize = size;
        return this;
    }

    public Color labelColor() {
        return _labelColor;
    }

    private GraphConfig labelColor(Color value) {
        _labelColor = value;
        return this;
    }

    public class Instance {

        public LineConfig.Instance newLine(String line) {
            return _parent.newLine(line);
        }
        public LineConfig.Instance newLine(String line, LineConfig value) {
            return _parent.newLine(line, value);
        }

        public Instance deleteLine(String line) {
            _parent.deleteLine(line);
            return this;
        }

        public LineConfig.Instance line(String line, LineConfig value) {
            return _parent.line(line, value);
        }

        public LineConfig.Instance line(String line) {
            return _parent.line(line);
        }

        public AxisConfig.Instance axis(int axis, AxisConfig value) {
            return _parent.axis(axis, value);
        }

        public AxisConfig.Instance axis(int axis) {
            return _parent.axis(axis);
        }

        public Color backColor() { return _parent.backColor(); }
        public Instance backColor(Color color) {
            _parent.backColor(color);
            return this;
        }

        public Color labelColor() { return _parent.labelColor(); }
        public Instance labelColor(Color color) {
            _parent.labelColor(color);
            return this;
        }

        public Color gridColor() { return _parent.gridColor(); }
        public Instance gridColor(Color color) {
            _parent.gridColor(color);
            return this;
        }

        public int gutterSize() { return _parent.gutterSize(); }
        public Instance gutterSize(int size) {
            _parent.gutterSize(size);
            return this;
        }

        public int marginSize() { return _parent.marginSize(); }
        public Instance marginSize(int size) {
            _parent.marginSize(size);
            return this;
        }
    }


}
