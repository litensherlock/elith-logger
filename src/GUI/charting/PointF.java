package GUI.charting;

/**
 * Point object with float precission.
 */
public class PointF {
    final public float x;
    final public float y;

    public PointF(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public PointF offset(float x,  float y) {
        return new PointF(this.x + x, this.y + y);
    }
}
