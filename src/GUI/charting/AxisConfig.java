package GUI.charting;

/**
 * Config for each axis in the charts.
 */
public class AxisConfig {
    private static final float DEFAULT_GRID_STEP = 10.0f;
    private static final int DEFAULT_GRID_SUB_STEP = 1;
    private static final float DEFAULT_MAX = 100.0f;
    private static final float DEFAULT_MIN = 0.0f;

    private AxisConfig parent = this;
    private float gridStep = DEFAULT_GRID_STEP;
    private float min = DEFAULT_MIN;
    private float max = DEFAULT_MAX;
    private String label = "Label";
    private String format = "%.1f";
    private int gridSubStep = DEFAULT_GRID_SUB_STEP;

    public Instance configure(GraphConfig.Instance config) {
        return new Instance(config);
    }

    public float min() {
        return min;
    }

    public AxisConfig min(float value) {
        min = value;
        return this;
    }

    public float max() {
        return max;
    }

    public AxisConfig max(float value) {
        max = value;
        return this;
    }

    public float gridStep() {
        return gridStep;
    }

    public AxisConfig gridStep(float value) {
        gridStep = value;
        return this;
    }

    public int gridSubSteps() {
        return gridSubStep;
    }

    public AxisConfig gridSubSteps(int value) {
        gridSubStep = value;
        return this;
    }

    public String label() {
        return label;
    }

    public AxisConfig label(String text) {
        label = text;
        return this;
    }

    public String format() {
        return format;
    }

    public AxisConfig format(String format) {
        this.format = format;
        return this;
    }

    public class Instance {
        private final GraphConfig.Instance config;

        public Instance(GraphConfig.Instance config) {
            this.config = config;
        }

        public float min() {
            return parent.min();
        }

        public Instance min(float value) {
            parent.min(value);
            return this;
        }

        public float max() {
            return parent.max();
        }

        public Instance max(float value) {
            parent.max(value);
            return this;
        }

        public float gridStep() {
            return parent.gridStep();
        }

        public Instance gridStep(float value) {
            parent.gridStep(value);
            return this;
        }

        /**
         * Can be used for future development
         * @returns the substeps for this axis.
         */
        public int gridSubSteps() {
            return parent.gridSubSteps();
        }

        /**
         * Can be used for future development
         * @returns this config instance.
         */
        public Instance gridSubSteps(int value) {
            parent.gridSubSteps(value);
            return this;
        }

        public String label() {
            return parent.label();
        }

        public Instance label(String text) {
            parent.label(text);
            return this;
        }

        public GraphConfig.Instance done() {
            return config;
        }

        /**
         * Can be used for future development
         * @returns the format for this axis.
         */
        public String format() {
            return parent.format();
        }

        /**
         * Can be used for future development
         * @returns this config instance
         */
        public Instance format(String format) {
            parent.format(format);
            return this;
        }
    }
}
