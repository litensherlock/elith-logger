package GUI.charting;

import common.Sensor;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-17
 */
public class ChartHandler {
    private Chart chart;

    private HashMap<String,DataLine> dataLines;

    private DataLineFactory factory;

    public ChartHandler(Chart chart) {

        this.chart = chart;
        dataLines = new HashMap<>();
        factory = new DataLineFactory();
    }

    public void addLine(Sensor sensor){
        DataLine line;

        line = dataLines.get(sensor.name());

        if (line == null){
            line = factory.createDataLine(sensor);
            dataLines.put(line.getName(),line);
        }

        chart.addDataLine(line);
    }

    public void removeLine(Sensor sensor){
        chart.removeDataLine(sensor.name());
    }

    public void resetChart() {
        for (String s : dataLines.keySet()) {
            chart.removeDataLine(s);
        }
        dataLines.clear();
    }
}
