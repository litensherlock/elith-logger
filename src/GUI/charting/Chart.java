package GUI.charting;

import common.Data;
import common.DataListener;
import common.Sensor;
import dataInput.ERin;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class Chart extends JPanel implements DataListener{

    private static final int GUTTER_SIZE = 60;
    private static final int CHART_WIDHT = 500;
    private static final int CHART_HEIGHT = 270;
    private static final float Y_AXIS_GRID_STEP = 0.5f;
    private static final float X_AXIS_GRID_STEP = 100;
    private static final float Y_AXIS_MAX = 2;
    private static final float Y_AXIS_MIN = -2;

    private Graph graph;
    private Map<String, Iterable<PointF>> dataSources;

    public Chart(int xAxisSize) {
        setPreferredSize(new Dimension(CHART_WIDHT, CHART_HEIGHT));

        dataSources = new HashMap<>();

        graph = new Graph();

        graph.configure()
            .gutterSize(GUTTER_SIZE)
            .axis(Axis.X)
                .label("Time")
                .min(0).max(xAxisSize)
                .gridStep(X_AXIS_GRID_STEP)
                .done()
            .axis(Axis.Y)
                .min(Y_AXIS_MIN).max(Y_AXIS_MAX)
                .gridStep(Y_AXIS_GRID_STEP)
                .done()
            .backColor(Color.WHITE)
            .gridColor(Color.LIGHT_GRAY)
            .labelColor(Color.BLACK)
            .marginSize(10);

        graph.setDataSources(dataSources);

        ERin.getInstance().addListener(this);
        setVisible(true);
    }

    @Override
    public void dataUpdated(Data data) {
        repaint();
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graph.update((Graphics2D) g, CHART_WIDHT, CHART_HEIGHT);
    }

    public void addDataLine(DataLine dataLine){
        graph.configure().newLine(dataLine.getName(),dataLine.getConfig());
        dataSources.put(dataLine.getName(),dataLine);

        if (graph.configure().axis(Axis.Y).max() < dataLine.getMax())
            graph.configure().axis(Axis.Y).max(dataLine.getMax());

        if (graph.configure().axis(Axis.Y).min() > dataLine.getMin())
            graph.configure().axis(Axis.Y).min(dataLine.getMin());
    }

    public void removeDataLine(String name){

        DataLine dataLine = (DataLine)(dataSources.remove(name));

        //there where no line of that name
        if (dataLine == null){
            return;
        }

        graph.configure().deleteLine(name);

        //find new max
        if (graph.configure().axis(Axis.Y).max() <= dataLine.getMax()){
            float max = 0;
            for (String s : dataSources.keySet()) {
                if (max < Data.getInstance().getSensorMax(Sensor.valueOf(s)))
                    max = Data.getInstance().getSensorMax(Sensor.valueOf(s));
            }
            graph.configure().axis(Axis.Y).max(max);
        }

        //find new min
        if (graph.configure().axis(Axis.Y).min() >= dataLine.getMin()){
            float min = 0;
            for (String s : dataSources.keySet()) {
                if (min > Data.getInstance().getSensorMin(Sensor.valueOf(s)))
                    min = Data.getInstance().getSensorMin(Sensor.valueOf(s));
            }
            graph.configure().axis(Axis.Y).min(min);
        }

    }
}