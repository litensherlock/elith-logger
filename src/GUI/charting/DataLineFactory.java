package GUI.charting;

import common.Sensor;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-17
 */
public class DataLineFactory {
    private static final float LINE_WIDTH = 3.0f;
    private static final int MAX = 255;
    private static final int HALF = 127;
    private int _colorIdx = 0;

    //colors are choosen to be visible.
    private final Color[] _readableColors = {
            new Color(0, MAX,0),
            new Color(0,0, MAX),
            new Color(MAX,0,0),
            new Color(MAX, HALF,0),
            new Color(0),
            new Color(MAX,0, MAX),
            new Color(0, MAX, MAX)
            };

    public DataLine createDataLine(Sensor sensor){

        DataLine line;

        ++_colorIdx;

        _colorIdx %= _readableColors.length;

        LineConfig config = new LineConfig()
                .color(_readableColors[_colorIdx])
                .width(LINE_WIDTH);

        line = new DataLine(sensor.name(), config);

        return line;
    }

}
