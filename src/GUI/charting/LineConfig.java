package GUI.charting;

import java.awt.*;

public class LineConfig {
    private LineConfig _parent = this;
    private Color _color = Color.RED;
    private float _width = 1.0f;

    public Instance configure(GraphConfig.Instance config) {
        return new Instance(config);
    }

    public Color color() { return _color; }
    public LineConfig color(Color value) {
        _color = value;
        return this;
    }

    public float width() { return _width; }
    public LineConfig width(float value) {
        _width = value;
        return this;
    }

    public class Instance {
        private final GraphConfig.Instance _config;

        public Instance(GraphConfig.Instance config) {
            _config = config;
        }

        public Color color() { return _parent.color(); }
        public Instance color(Color value) {
            _parent.color(value);
            return this;
        }

        public float width() { return _parent.width(); }
        public Instance width(float value) {
            _parent.width(value);
            return this;
        }

        public GraphConfig.Instance done() {
            return _config;
        }
    }
}
