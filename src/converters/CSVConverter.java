package converters;


/**
 * Created by Jakob Lövhall on 2014-05-04.
 */
public class CSVConverter implements ConvertStrategy{
    private int [] x;
    private float [] y;

    public CSVConverter(int[] x, float[] y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public float convert(int n) {
        int i;

        //these are the accepted math terms used in the strait line equation. y = k*x + m
        float k,m;

        for (i = 0; i < x.length; i++) {

            //on the right of value x[i]
            if (n < x[i]) {
                break;
            }
        }

        //if i is the last item x the array. build a line from the point before it.
        if (i == x.length){
            k = findK(x[i - 1], x[i],y[i - 1], y[i]);
        } else { //else build a line from this point to the next point.
            k = findK(x[i], x[i + 1],y[i], y[i + 1]);
        }

        m = findM(y[i], k, x[i]);

        GenericConverter gc = new GenericConverter(new ConvertParameters(0,m,1,k,true));

        return gc.convert(n);
    }

    /**
     * y/x = k
     */
    private float findK(float x1, float x2, float y1, float y2){
        return (y2 - y1) / (x2 - x1);
    }

    /**
     * y = kx + m  <=> y-kx = m
     */
    private float findM(float y, float k, float x){ // k is the accepted term in math.
        return y - k * x;
    }
}
