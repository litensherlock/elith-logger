package converters;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob lövhall
 * Date: 2013-10-21
 * Time: 22:26
 */
public interface ConvertStrategy {
    public float convert(int n);
}
