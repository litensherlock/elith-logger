package converters;

import common.Config;
import common.Sensor;


/**
 * Created with IntelliJ IDEA.
 * User: Jakob lövhall
 * Date: 2013-10-21
 * Time: 22:42
 */
public class Converter {
    private ConvertStrategy strategy;

    public Converter(Sensor sensor) {
        strategy = Config.getInstance().getConverterFactory().createConverter(sensor);
    }

    public float convert(int n){
        return strategy == null ? 0 : strategy.convert(n);
    }
}
