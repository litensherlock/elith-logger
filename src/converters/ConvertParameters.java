package converters;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-28
 */
public class ConvertParameters {
    private final float addBefore;
    private final float addAfter;
    private final float div;
    private final float mult;
    private final boolean sign;

    public ConvertParameters(float addBefore, float addAfter, float div, float mult, boolean sign) {
        this.addBefore = addBefore;
        this.addAfter = addAfter;
        this.div = div;
        this.mult = mult;
        this.sign = sign;
    }

    public float getMult() {
        return mult;
    }

    public boolean isSigned() {
        return sign;
    }

    public float getAddBefore() {
        return addBefore;
    }

    public float getAddAfter() {
        return addAfter;
    }

    public float getDiv() {
        return div;
    }
}
