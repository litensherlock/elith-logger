package converters;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-28
 */
public class GenericConverter implements ConvertStrategy{
    private final ConvertParameters p;

    public GenericConverter(ConvertParameters p) {
        this.p = p;
    }

    /**
     * Cast the int to a short if the number should be signed.
     * @param n a integer with 16bits used.
     * @return a float calculated by: f(n) = (a + n*b /c ) + d
     */
    @Override
    public float convert(int n) {
        //by casting the int to a short the sign bit is set correctly. The int can contain a 16bit unsigned value so this should not always be done.
        if (p.isSigned())
            n = (short)n;

        return (p.getAddBefore() + n*p.getMult()) / p.getDiv() + p.getAddAfter();
    }
}


