package converters;

import common.Sensor;

import java.util.EnumMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-28
 */
public class ConverterFactory {
    private Map<Sensor,ConvertParameters> parameterMap;
    private Map<Sensor,CSVConverter> CSVMap; // Comma separated value map

    public ConverterFactory() {
        parameterMap = new EnumMap<>(Sensor.class);
        CSVMap = new EnumMap<>(Sensor.class);
    }

    public Map<Sensor, ConvertParameters> getParameterMap() {
        return parameterMap;
    }

    public Map<Sensor, CSVConverter> getCSVMap() {
        return CSVMap;
    }

    public ConvertStrategy createConverter(Sensor sensor) {

        if (CSVMap.get(sensor) != null) {
            return CSVMap.get(sensor);
        }

        ConvertParameters p = parameterMap.get(sensor);

        //default
        if (p == null) {
            p = new ConvertParameters(0, 0, 1, 1, false);
        }

        return new GenericConverter(p);
    }
}
