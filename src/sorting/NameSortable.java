package sorting;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-28
 */
public interface NameSortable {
    public String getName();
}
