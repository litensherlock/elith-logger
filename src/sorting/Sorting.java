package sorting;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-28
 */

/**
 * Used for sorting the XML arguments.
 * Note that the list can not be set for only <NameSortables> because some list need to be used for other types.
 * All lists that is sent in do have Namesortable's in them but needs to be defined as something else for the XML marshal.
 */
public final class Sorting {
    private Sorting() {
    }

    //this list can not be only for <NameSortables> because some list need to be used for other types. all lists is also NameSortable.
    public static void addSorted(List list, NameSortable sortable){
        boolean done = false;

        String unknownS;

        unknownS = sortable.getName();

        for (int j = 0; j < list.size(); j++) {
            String curS = ((NameSortable) list.get(j)).getName();

            for (int k = 0; k < curS.length() && k < unknownS.length(); k++) {
                Character unknonwC = unknownS.charAt(k);
                Character curC = curS.charAt(k);

                //if they are equal continue with the next character.
                if (curC.compareTo(unknonwC) == 0)
                    continue;

                if (curC.compareTo(unknonwC) > 0){
                    list.add(j,sortable);
                    done = true;
                    break;
                }

                if (curC.compareTo(unknonwC) < 0)
                    break;
            }
            if (done)
                break;
        }

        if (!done)
            list.add(sortable);
    }
}
