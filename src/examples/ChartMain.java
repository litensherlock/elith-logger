package examples;

import GUI.charting.Axis;
import GUI.charting.Graph;
import GUI.charting.PointF;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 * example of how to use the charts
 */
public final class ChartMain {

    private static final int GUTTER_SIZE = 60;
    private static final float ACC_X_WIDTH = 2.5f;
    private static final float ACC_Y_WIDTH = 5.0f;
    private static final float ACC_Z_WIDTH = 3.0f;
    private static final int WIDTH = 600;
    private static final int HEIGHT = 600;

    private ChartMain() {
    }

    /**
     * Example of how to use the charts. Run to get a example PNG
     */
    public static void main(String[] args) {
        Graph g = new Graph();

        g.configure()
            .gutterSize(GUTTER_SIZE)
            .axis(Axis.X)
                .label("mdB")
                .min(0).max(1000)
                .gridStep(100)
                .done()
            .axis(Axis.Y)
                .label("Seconds")
                .min(0).max(100)
                .gridStep(10)
                .done()
            .newLine("accX")
                .color(Color.RED)
                .width(ACC_X_WIDTH)
                .done()
            .newLine("accY")
                .color(Color.BLUE)
                .width(ACC_Y_WIDTH)
                .done()
            .newLine("accZ")
                .color(Color.GREEN)
                .width(ACC_Z_WIDTH)
                .done()
            .backColor(Color.WHITE)
            .gridColor(Color.LIGHT_GRAY)
            .labelColor(Color.BLACK);

        Map<String, Iterable<PointF>> data = new HashMap<>();
        for (String s : new String[]{"accX", "accY", "accZ"}) {

            List<PointF> ps = new ArrayList<>();
            ps.add(new PointF(0,0));
            Random r = new Random();

            for (int i = 1; i < 40; i++) {
                ps.add(new PointF(ps.get(i-1).x + 20, ps.get(i-1).y + r.nextInt(12) - 4));
            }
            data.put(s, ps);
        }

        BufferedImage img = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);

        g.setDataSources(data);

        g.update((Graphics2D) img.getGraphics(), WIDTH, HEIGHT);

        try {
            ImageIO.write(img, "png", new File("chart.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
