package examples;

import common.parsing.ConvertParser;
import converters.ConvertParameters;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-06-02
 */
public final class ConvertParserTest {
    private ConvertParserTest() {
    }

    public static void main(String[] args) {
        String s = "f(n) = (2.0 + 3.0*n)/7.0 + 1";
        String s2 = "f(n) = n/6.0";

        String sTest = "f(n) = ( 2.0 + 3.0 * n ) / 10.0 + 1";

        ConvertParameters cp = new ConvertParameters(0,1,1,1,false);

        System.out.println(ConvertParser.toString(cp));
    }
}
