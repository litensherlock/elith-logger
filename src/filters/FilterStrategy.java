package filters;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob lövhall
 * Date: 2013-12-26
 * Time: 20:57
 */

/**
 * all filters gets a float as input and returns a float as output
  */

public interface FilterStrategy {
    public float filter(float in);
}
