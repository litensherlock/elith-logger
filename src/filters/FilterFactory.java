package filters;

import common.Sensor;
import dataStructures.XMLHelpers.filters.FilterType;

import java.util.EnumMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-28
 */
public class FilterFactory {
    private Map<Sensor,FilterParameters> parameterMap;

    public FilterFactory() {
        parameterMap = new EnumMap<>(Sensor.class);
    }

    public Map<Sensor, FilterParameters> getParameterMap() {
        return parameterMap;
    }

    public FilterStrategy createFilter(Sensor sensor){
        FilterParameters fp = parameterMap.get(sensor);

        FilterStrategy filterStrategy;

        if (fp != null)
            filterStrategy = getFilter(fp);
        else //default
            filterStrategy = new DefaultFilter();

        return filterStrategy;
    }

    private FilterStrategy getFilter(FilterParameters fp){
        FilterStrategy fs;

        if (fp.getType() == FilterType.FIR){
            fs = new FIRFilter(fp.getAverage());
        } else
            fs = new IIRFilter(fp.getScale());

        return fs;
    }
}
