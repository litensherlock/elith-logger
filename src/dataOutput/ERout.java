package dataOutput;

import common.Data;
import common.DataListener;
import dataInput.ERin;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob lövhall
 * Date: 2013-11-27
 * Time: 10:38
 */

/**
 * Main class for handeling data output from the program.
 */
public class ERout implements DataListener{
    private SerialFileWriter serialLogger;
    private SerialCOMWriter comWriter;
    private TextWriter textWriter;

    private static ERout myself = null;

    private boolean _binLog;
    private boolean _textLog;

    /**
     * singelton getter
     */
    public synchronized static ERout getInstance(){
        if(myself == null)
            myself = new ERout();

        return myself;
    }

    private ERout() {
        _binLog = false;
        _textLog = false;
        serialLogger = null;
        comWriter = null;
        textWriter = null;
    }

    /**
     * stops the logging to a binary file and removes the listener if nobody else (the text logger) are currently active
     */
    public void stopBinLogging(){
        if(_binLog){
            try {
                if (serialLogger != null)
                    serialLogger.getOutputStream().close();
            } catch (IOException ignored) {//assumed to be closed if we can not close it.

            }
        }
        _binLog = false;

        //check if someone els is logging
        if (!_textLog)
            ERin.getInstance().removeListener(this);
    }

    public void openCOMOutputStream(OutputStream os){
        comWriter = new SerialCOMWriter(new DataOutputStream(os));
    }

    public void closeCOMOutputStream(){
        try {
            comWriter.getOutputStream().close();
        } catch (IOException ignored) { // asumed to allready be closed if it can not be closed
        }
        comWriter = null;
    }

    /**
     * used for sending byte arrays via the low level protocol.
     * Can be used for future development. Sending more complicated data then one Short at the time for example.
     * @param bytes byte array to be sent
     * @throws IOException
     */
    public void sendPackage(byte[] bytes) throws IOException {
        comWriter.write(bytes);
    }

    public void sendPackage(short value) throws IOException {
        comWriter.write(value);
    }

    /**
     * starts the logging to binary file and adds a listener if nobody els is currently active
     */
    public void startBinLogging(){

        //yyyyMMdd_HHmmss. add or remove as you want
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(Calendar.getInstance().getTime());

        try {
            FileOutputStream fos=new FileOutputStream(timeStamp + ".bin",true);

            DataOutputStream dos = new DataOutputStream(fos); // the output stream shall remande open after this function call

            serialLogger = new SerialFileWriter(dos);

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("could not find the file " + e);
        }

        if (!(_binLog || _textLog))
            ERin.getInstance().addListener(this);

        //done last to make sure no timer can try and write to the stream before it is initialized
        _binLog = true;
    }

    /**
     * creates a new txt file with current date and time as filename.
     * then creates a new TextWriter to do the logging
     */
    public void startTextLogging(){

        //yyyyMMdd_HHmmss. add or remove as you want
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(Calendar.getInstance().getTime());

        try {
            FileOutputStream fos=new FileOutputStream(timeStamp + ".txt",true);

            DataOutputStream dos = new DataOutputStream(fos); // the output stream shall remande open after this function call

            textWriter = new TextWriter(dos);

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("could not create the file " + e);
        }

        if (!(_binLog || _textLog))
            ERin.getInstance().addListener(this);

        //done last to make sure no timer can try and write to the stream before it is initialized
        _textLog = true;
    }

    /**
     * stops the logging to a txt file and removes the listener if nobody else (the binary logger) are currently active
     */
    public void stopTextLogging(){
        if(_textLog){
            try {
                if (textWriter != null)
                    textWriter.getOutputStream().close();
            } catch (IOException ignored) {//assumed to be closed if we can not close it.

            }
        }

        _textLog = false;

        if (!(_binLog || _textLog))
            ERin.getInstance().removeListener(this);

    }

    /**
     * writes to all active loggers.
     */
    public void store(){
        if (_textLog)
            textWriter.store();

        if(_binLog)
            serialLogger.store();
    }

    public boolean isBinLogging() {
        return _binLog;
    }

    public boolean isTextLogging() {
        return _textLog;
    }

    @Override
    public void dataUpdated(Data data) {
        store();
    }

}
