package dataOutput;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by Jakob Lövhall on 2014-03-08.
 */
public class SerialCOMWriter extends SerialWriter {

    private static final int BYTE_MASK = 0xff;

    public SerialCOMWriter(DataOutputStream dos) {
        super(dos);
    }

    public void write(short value) throws IOException {
        byte[] bytes = new byte[2];
        bytes[1] = (byte) (value & BYTE_MASK);
        bytes[0] = (byte) ((value << 8) & BYTE_MASK);
        write(bytes);
    }
}
