package dataOutput;
import common.Data;
import common.Flags;
import common.Sensor;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob lövhall
 * Date: 2013-11-26
 * Time: 23:44
 */
public class SerialFileWriter extends SerialWriter{

    public SerialFileWriter(DataOutputStream dos) throws IOException {
        super(dos);
        writeHeader();
    }

    // write a version number. in version 1 we one integer for the number of sensors stored then the sensors then the data starts
    private void writeHeader() throws IOException {
        int length = Data.getInstance().getProtocol().length;

        int version = 1;

        dos.writeInt(version);

        dos.writeInt(length);

        for (Sensor sensor : Data.getInstance().getProtocol()) {
            dos.writeInt(Flags.getFlag(sensor));
        }
    }

    public void store(){
        try {
            write(Data.getInstance().toRawBytes());
        } catch (IOException e) {
            System.out.println("could not write to file " + e);
        }
    }
}
