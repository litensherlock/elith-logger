package dataOutput;
import common.DataPacker;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob lövhall
 * Date: 2013-11-26
 * Time: 23:44
 */

/**
 * The parent to the serial writer classes.
 */
class SerialWriter {
    protected DataOutputStream dos;

    protected SerialWriter(DataOutputStream dos){
        this.dos = dos;
    }

    public DataOutputStream getOutputStream() {
        return dos;
    }

    public void write(byte[] bytes) throws IOException {
       dos.write(DataPacker.pack(bytes));
    }

    public void writeRaw(byte[] bytes) throws IOException {
        dos.write(bytes);
    }
}
