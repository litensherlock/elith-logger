package dataOutput;

import common.Data;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob lövhall
 * Date: 2013-11-27
 * Time: 10:50
 */
public class TextWriter {
    private DataOutputStream dos;
    private Data data;

    public TextWriter(DataOutputStream dos) {
        this.dos = dos;
        data = Data.getInstance();
    }

    public DataOutputStream getOutputStream() {
        return dos;
    }

    //stores all sensors on a line with name first then raw value
    public void store() {
        try {
            dos.write(data.toRawString().getBytes());
        } catch (IOException e) {
            System.out.println("could not write to file " + e);
        }
    }
}
