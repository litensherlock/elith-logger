package dataStructures;

import GUI.charting.PointF;
import common.Data;
import common.DataListener;
import common.Sensor;
import filters.FilterStrategy;

import java.util.EnumMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-21
 */
public class ChartbufferHandler implements DataListener{
    private EnumMap<Sensor, RingBuffer> _buffers;
    private EnumMap<Sensor, RingBuffer> _buffersWithFilters;
    private Map<Sensor, FilterStrategy> _filters;
    private Sensor[] _sensors;
    private int _bufferSize;

    public ChartbufferHandler(Sensor[] sensors, int bufferSize) {
        _bufferSize = bufferSize;

        _buffers = new EnumMap<>(Sensor.class);
        _filters = new EnumMap<>(Sensor.class);
        _buffersWithFilters = new EnumMap<>(Sensor.class);

        for (Sensor sensor : sensors) {
            _buffers.put(sensor, new RingBuffer(bufferSize));
        }
        _sensors = sensors;
    }

    @Override
    public void dataUpdated(Data data) {
        for (Sensor sensor : _sensors) {
            float f = data.getSensorValue(sensor);
            final int currentTime = data.getCurrentTime();

            _buffers.get(sensor).add(new PointF(currentTime, f));

            //if the current sensor got a filter the filtered buffer should be updated as well.
            RingBuffer r = _buffersWithFilters.get(sensor);
            if (r != null){
                r.add(new PointF(currentTime,_filters.get(sensor).filter(f)));
            }
        }
    }

    /**
     * Copies the data and filters it to a new filter buffer.
     * This is done via a indexed for loop because the iterator offsets the buffer to make it roll in the charts.
     */
    public void setFilter(Sensor sensor, FilterStrategy filter){
        if (_buffersWithFilters.containsKey(sensor)){
            _buffersWithFilters.remove(sensor);
        }

        RingBuffer ring = new RingBuffer(_bufferSize);

        RingBuffer raw = _buffers.get(sensor);

        for (int i = 0; i < raw.size(); i++) {
            PointF pf = raw.get(i);
            ring.add(new PointF(pf.x,filter.filter(pf.y)));
        }

        _buffersWithFilters.put(sensor,ring);
        _filters.put(sensor,filter);
    }

    public void removeFilters(Sensor sensor){
        _buffersWithFilters.remove(sensor);
        _filters.remove(sensor);
    }

    public RingBuffer getBuffer(Sensor sensor){
        RingBuffer r;

        r = _buffersWithFilters.get(sensor);
        if (r != null )
            return r;
        else
            return _buffers.get(sensor);
    }
}
