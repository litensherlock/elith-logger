package dataStructures;

import GUI.charting.PointF;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-06
 * Time: 21:50
 */
class AnchorItem extends ListItem {

    AnchorItem() {
        next = this;
        prev = this;
    }

    @Override
    public PointF getData() {
        if (next.equals(this))
            return new PointF(0,0);
        return next.getData();
    }

    @Override
    public PointF getData(int idx) {
        if (next.equals(this))
            return new PointF(0,0);
        return next.getData(idx);
    }
}
