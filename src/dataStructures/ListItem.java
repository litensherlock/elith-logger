package dataStructures;

import GUI.charting.PointF;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-07
 * Time: 22:35
 */

/**
 * the items which the RingBuffer consists of
 */
abstract class ListItem {
    protected ListItem next;
    protected ListItem prev;

    protected ListItem() {
        next = null;
        prev = null;
    }

    public ListItem getNext() {
        return next;
    }

    public void setNext(ListItem next) {
        this.next = next;
    }

    public ListItem getPrev() {
        return prev;
    }

    public void setPrev(ListItem prev) {
        this.prev = prev;
    }

    public abstract PointF getData();
    public abstract PointF getData(int idx);
}
