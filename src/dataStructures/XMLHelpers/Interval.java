package dataStructures.XMLHelpers;

import common.Sensor;
import sorting.NameSortable;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-23
 */

/**
 * XML helper class
 */
public class Interval implements NameSortable {
    @XmlAttribute
    public Sensor sensor = null;
    @XmlAttribute
    public float min;
    @XmlAttribute
    public float max;

    public Interval(Sensor sensor, float min, float max) {
        this.sensor = sensor;
        this.min = min;
        this.max = max;
    }

    //used by the XML marshal
    public Interval() {
    }

    @Override
    public String getName() {
        return sensor.name();
    }
}
