package dataStructures.XMLHelpers;

import common.Sensor;
import sorting.Sorting;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-23
 */

/**
 * XML helper class. Unused functions are used by the marshal.
 */
public class Intervals {
    private List<Interval> intervals;

    public Intervals() {
        intervals = new ArrayList<>();
    }

    public void add(Interval interval){
        Sorting.addSorted(intervals,interval);
    }

    /**
     * used by the XML reader
     * @param in
     */
    public void setIntervals(Interval [] in){
        intervals = new ArrayList<>();
        Collections.addAll(intervals, in);
    }

    /**
     * used by the XML writer
     */
    public Interval[] getIntervals(){
        Interval[] ret = new Interval[intervals.size()];
        intervals.toArray(ret);
        return ret;
    }

    public Map<Sensor,Interval> getIntervalMap(){
        Map<Sensor,Interval> h = new EnumMap<>(Sensor.class);
        for (Interval interval : intervals) {
            h.put(interval.sensor,interval);
        }
        return h;
    }
}
