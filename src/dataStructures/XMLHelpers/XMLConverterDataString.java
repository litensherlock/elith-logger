package dataStructures.XMLHelpers;

import common.Sensor;
import sorting.NameSortable;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-28
 */

/**
 * XML helper class.
 */
public class XMLConverterDataString implements NameSortable {
    @XmlAttribute
    public Sensor sensor = null;
    @XmlAttribute
    public boolean isSigned;
    @XmlAttribute
    public String function = null;

    //used by the XML marshal
    public XMLConverterDataString() {
    }

    public XMLConverterDataString(Sensor sensor, boolean isSigned, String function) {
        this.sensor = sensor;
        this.isSigned = isSigned;
        this.function = function;
    }

    @Override
    public String getName() {
        return sensor.name();
    }
}
