package dataStructures.XMLHelpers;

import sorting.Sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-28
 */

/**
 * XML helper class.
 */
public class XMLConvertersString {
    private List<XMLConverterDataString> converters;

    public XMLConvertersString() {
        converters = new ArrayList<>();
    }

    public void add(XMLConverterDataString dataString){
        Sorting.addSorted(converters, dataString);
    }

    public void setConverters(XMLConverterDataString[] in){
        converters = new ArrayList<>();
        Collections.addAll(converters, in);
    }

    public XMLConverterDataString[] getConverters(){
        XMLConverterDataString[] ret = new XMLConverterDataString[converters.size()];
        converters.toArray(ret);
        return ret;
    }
}
