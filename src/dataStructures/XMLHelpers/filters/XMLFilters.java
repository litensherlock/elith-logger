package dataStructures.XMLHelpers.filters;

import sorting.Sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-28
 */
public class XMLFilters {
    private List<XMLFilterData> filters;

    public XMLFilters() {
        filters = new ArrayList<>();
    }

    public void add(XMLFilterData xmlFilterData){
        Sorting.addSorted(filters, xmlFilterData);
    }

    public void setFilters(XMLFilterData[] in){
        filters = new ArrayList<>();
        Collections.addAll(filters, in);
    }

    public XMLFilterData[] getFilters(){
        XMLFilterData[] ret = new XMLFilterData[filters.size()];
        filters.toArray(ret);
        return ret;
    }
}
