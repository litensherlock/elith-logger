package dataStructures.XMLHelpers.filters;

import common.Sensor;
import sorting.NameSortable;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-28
 */

/**
 * XML helper class.
 */
public class XMLFilterData implements NameSortable {
    @XmlAttribute
    public Sensor sensor = null;
    @XmlAttribute
    public float value;
    @XmlAttribute
    public FilterType type = null;

    //used by the XML marshal
    public XMLFilterData() {
    }

    public XMLFilterData(Sensor sensor, float value, FilterType type) {
        this.sensor = sensor;
        this.value = value;
        this.type = type;
    }

    @Override
    public String getName() {
        return sensor.name();
    }
}
