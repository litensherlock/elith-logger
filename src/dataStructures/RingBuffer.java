package dataStructures;

import GUI.charting.PointF;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-06
 * Time: 02:11
 */
public class RingBuffer implements Iterable<PointF> {

    private AnchorItem _Anchor;
    private int _size;
    private int _currentSize;

    /**
     * creates a ring buffer of size size
     * @param size the size of the buffer
     */
    public RingBuffer(int size) {
        _Anchor = new AnchorItem();
        _size = size;
        _currentSize = 0;
    }

    /**
     * @return the size of the list
     */
    public int size() {
        return _currentSize;
    }

    /**
     * adds a new item last in the buffer and removes the first item
     * @param p the data that should be stored
     */
    public void add(PointF p){
        addLast(p);
        if (_currentSize > _size)
            removeFirst();
    }


    /**
     * adds a item to the end of the list
     * @param p the item that is added last to the list
     */
    private void addLast(PointF p){
        DataItem newItem = new DataItem();
        newItem.setData(p);

        newItem.setPrev(_Anchor.getPrev());
        newItem.setNext(_Anchor);

        _Anchor.getPrev().setNext(newItem);

        _Anchor.setPrev(newItem);
        _currentSize++;
    }

    /**
     * removes the first item in the list
     */
    private void removeFirst(){
        _Anchor.setNext(_Anchor.getNext().getNext());
        _Anchor.getNext().setPrev(_Anchor);
        _currentSize--;
    }

    public PointF get(int idx){
        if (_currentSize == 0)
            return new PointF(0,0);
        return _Anchor.getData(idx);
    }

    public PointF getLast(){
        return _Anchor.prev.getData();
    }

    /**
     *
     * @return iterator<PointF> that goes through the list and accounts for the offset
     */
    @Override
    public Iterator<PointF> iterator() {
        return new Iterator<PointF>() {
            private ListItem cur = _Anchor;

            @Override
            public boolean hasNext() {
                return !cur.getNext().equals(_Anchor);
            }

            @Override
            public PointF next() {
                cur = cur.getNext();
                if (cur.equals(_Anchor))
                    throw new NoSuchElementException("end of the buffer");

                PointF p = cur.getData();
                return p.offset(-_Anchor.getNext().getData().x,0);
            }

            @Override
            public void remove() {}
        };
    }
}
