package dataStructures;

import GUI.charting.PointF;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-06
 * Time: 21:24
 */
class DataItem extends ListItem {
    private PointF data;

    DataItem() {
        data = new PointF(0,0);
    }

    public PointF getData(int idx) {
        if (idx <= 0)
            return data;

        return next.getData(idx - 1);
    }

    public void setData(PointF data) {
        this.data = data;
    }

    @Override
    public PointF getData() {
        return data;
    }
}
