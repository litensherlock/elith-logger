package common;

import converters.Converter;

/**
 *
 * @author jonasbromo
 * edited by jakob lövhall
 */
public class ERDataField {
    private static final int BYTE_LENGTH = 8;
    private static final int BYTE_MASK = 0xff;

    private int value = 0;
    private final Sensor _sensor;
    private Converter converter;
    
    public ERDataField(Sensor sensor) {
        _sensor = sensor;
        converter = new Converter(sensor);
    }

    //cast to short to get the sign bit
    public void setValue(byte highByte, byte lowByte) {
        value = (short) ( (highByte << BYTE_LENGTH) + lowByte);
    }

    public void setValue(short value) {
        this.value = value;
    }

    public float getValue() {
        return converter.convert(value);
    }

    public int getRawValue() {
        return value;
    }

    public byte getHighByte() {
        return (byte)(value >> BYTE_LENGTH);
    }
    
    public byte getLowByte() {
        return (byte)(value & BYTE_MASK);
    }

    public Sensor getSensor() {
        return _sensor;
    }
}
