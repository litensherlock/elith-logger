package common;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob lövhall
 * Date: 2013-10-20
 * Time: 22:46
 */

//used in the XML file so it is not wanted to have only upper-case
public enum Sensor {
    OilPressure,
    RPM,
    AccX,
    AccY,
    AccZ,
    NumValidPackets,
    NumFailedPackets,
    Throttle,
    Temp,
    MAF
}
