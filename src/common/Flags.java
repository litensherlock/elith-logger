package common;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob lövhall
 * Date: 2013-11-16
 * Time: 10:43
 */

/**
 * used to code and decode sensors to the binary file.
 * add more sensors here to be able to store them in binary files.
 */
public final class Flags {
    final private static int OIL_PRESSURE = 1;
    final private static int RPM = 2;
    final private static int ACC_X = 3;
    final private static int ACC_Y = 4;
    final private static int ACC_Z = 5;
    final private static int NUM_VALID_PACKETS = 6;
    final private static int NUM_FAILED_PACKETS = 7;
    final private static int THROTTLE = 8;
    final private static int TEMP = 9;
    final private static int MAF = 10;

    private Flags() {
    }

    public static Sensor getSensor(int fl){
        switch (fl){
            case 1:
                return Sensor.OilPressure;
            case 2:
                return Sensor.RPM;
            case 3:
                return Sensor.AccX;
            case 4:
                return Sensor.AccY;
            case 5:
                return Sensor.AccZ;
            case 6:
                return Sensor.NumValidPackets;
            case 7:
                return Sensor.NumFailedPackets;
            case 8:
                return Sensor.Throttle;
            case 9:
                return Sensor.Temp;
            case 10:
                return Sensor.MAF;
            default:
                return null;
        }
    }

    public static int getFlag(Sensor sensor) {
        switch (sensor) {
            case OilPressure:
                return OIL_PRESSURE;
            case RPM:
                return RPM;
            case AccX:
                return ACC_X;
            case AccY:
                return ACC_Y;
            case AccZ:
                return ACC_Z;
            case NumValidPackets:
                return NUM_VALID_PACKETS;
            case NumFailedPackets:
                return NUM_FAILED_PACKETS;
            case Throttle:
                return THROTTLE;
            case Temp:
                return TEMP;
            case MAF:
                return MAF;
        }
        return 0;
    }
}
