package common;

import GUI.menues.ProtocolChangeListener;
import dataInput.ERin;
import dataStructures.ChartbufferHandler;
import dataStructures.RingBuffer;

import java.util.EnumMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob lövhall
 * Date: 2013-10-01
 * Time: 17:09
 */

public class Data implements ProtocolChangeListener{

    //chart x-size
    private static final int BUFFER_SIZE = 160; //todo connect to the charts

    private static Data mySelf = null;

    private Config config;
    private ChartbufferHandler chartbufferHandler;

    private Map<Sensor,ERDataField> dataMap;

    private int _currentTime;

    private ERDataField[] _cur; // used for the serialization of the data.

    /**
     * singleton getter for this object to make sure that the same configuration is use throe out the program.
     * @return a single (all ways the same) instance of this class
     */
    public synchronized static Data getInstance(){
        if(mySelf == null)
            mySelf = new Data();

        return mySelf;
    }

    private Data(){
        config = Config.getInstance();
        chartbufferHandler = new ChartbufferHandler(config.getProtocol(), BUFFER_SIZE);
        ERin.getInstance().addListener(chartbufferHandler);

        _currentTime = 0;

        dataMap = new EnumMap<>(Sensor.class);

        ERDataField[] cur = new ERDataField[config.getProtocol().length];

        for(int i = 0; i < config.getProtocol().length; i++){
            cur[i] = new ERDataField(config.getProtocol()[i]);
        }

        _cur = cur;

        Config.getInstance().addProtocolChangeListener(this);
    }

    public static int getBufferSize() {
        return BUFFER_SIZE;
    }

    private void setChartBuffers(Sensor[] sensors){
        ERin.getInstance().removeListener(chartbufferHandler);
        chartbufferHandler = new ChartbufferHandler(sensors, BUFFER_SIZE);
        ERin.getInstance().addListener(chartbufferHandler);
    }

    /**
     * get a new set of data
     * @param newDataFields the new data
     */
    public void setNewDataFields(ERDataField[] newDataFields){
       _cur = newDataFields;
        dataMap.clear();
        for (ERDataField dataField : newDataFields) {
            dataMap.put(dataField.getSensor(),dataField);
        }
    }

    public Sensor[] getProtocol() {
        return config.getProtocol();
    }

    public int getNumberOfSensors() {
        return config.getProtocol().length;
    }

    public ChartbufferHandler getChartbufferHandler() {
        return chartbufferHandler;
    }

    public RingBuffer getChartbuffer(Sensor sensor) {
        return chartbufferHandler.getBuffer(sensor);
    }

    /**
     * @return a byte array with the raw values of all sensors
     */
    public byte[] toRawBytes(){
        byte[] bytes = new byte[config.getProtocol().length*2];

        int bidx = 0;

        for (int i = 0; i < config.getProtocol().length; i++) {
            bytes[bidx] = _cur[i].getHighByte();
            bidx++;
            bytes[bidx] = _cur[i].getLowByte();
            bidx++;
        }

        return bytes;
    }

    /**
     * @return a string with sensor names and raw sensor values
     */
    public String toRawString() {
        StringBuilder sb=new StringBuilder();

        for(ERDataField field : _cur){
            sb.append(field.getSensor());
            sb.append(" ");
            sb.append(field.getRawValue());
            sb.append(" ");
        }
        sb.append("\n");
        return sb.toString();
    }

    /**
     * @return a string with sensor names and converted values
     */
    public String toString(){
        StringBuilder sb=new StringBuilder();

        for(ERDataField field : _cur){
            sb.append(field.getSensor());
            sb.append(" ");
            sb.append(field.getValue());
            sb.append(" ");
        }
        sb.append("\n");
        return sb.toString();
    }

    public ERDataField[] getSensors() {
        return _cur;
    }

    /**
     * gets the sensor from the data map and returns the value if the sensor is in the map other wise
     * zero if the sensor is not in in the data map.
     * @param sensor the sensor of witch the value is returned
     * @return the value of the sensor or 0 if there is no such sensor
     */
    public float getSensorValue(Sensor sensor){ // todo do something that takes the angle of the accelerometers in to account.
        float f = 0;

        ERDataField dataField = dataMap.get(sensor);
        if (dataField != null){
            f = dataField.getValue();
        }
        return f;
    }

    public void setCurrentTime(int time){
        _currentTime = time;
    }

    public int getCurrentTime(){
        return _currentTime;
    }

    public float getSensorMax(Sensor sensor){
        return config.getSensorMax(sensor);
    }

    public float getSensorMin(Sensor sensor){
        return config.getSensorMin(sensor);
    }

    /**
     * change the working protocol
     * @param protocol
     */
    @Override
    public void protocolChange(Sensor[] protocol) {
        setChartBuffers(protocol);
    }
}
