package common;

import GUI.menues.ProtocolChangeListener;
import common.parsing.ConvertParser;
import converters.CSVConverter;
import converters.ConvertParameters;
import converters.ConverterFactory;
import dataStructures.XMLHelpers.*;
import dataStructures.XMLHelpers.filters.FilterType;
import dataStructures.XMLHelpers.filters.XMLFilterData;
import dataStructures.XMLHelpers.filters.XMLFilters;
import filters.FilterFactory;
import filters.FilterParameters;

import javax.xml.bind.JAXB;
import javax.xml.bind.annotation.XmlAttribute;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-17
 */

/**
 * this class is used to get all kind of configuration for the rest of the program.
 */
public class Config {
    private static final String CONFIG_XML = "config.xml";
    private static final int DEFAULT_TEMP_MAX = 125;
    private static final int DEFAULT_TEMP_MIN = 0;
    private static final int DEFAULT_ACCEL_MAX = 2;
    private static final int DEFAULT_ACCEL_MIN = -2;
    private static final int DEFAULT_OIL_PRESSURE_MAX = 4;
    private static final int DEFAULT_OIL_PRESSURE_MIN = 0;

    private static Config myself = null;
    private Sensor[] protocol;
    private Sensor[] _wirelessProtocol;
    private Map<Sensor, Interval> intervalMap;
    private float[] offsets;
    private ConverterFactory converterFactory;
    private FilterFactory filterFactory;
    private List<ProtocolChangeListener> protocolChangeListeners;

    private Config() {
        protocolChangeListeners = new ArrayList<>();
        protocol = null;
        _wirelessProtocol = null;
        intervalMap = null;
        offsets = null;
        converterFactory = null;
        filterFactory = null;

        try {
            readXMLconfigFile();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.out.println("Could not load configurations. Setting up defaults");
            setPartDefaults();
        }
        writeXMLConfigFile();
    }

    /**
     * reads the xml file in to the xml helper class XMLConfig and then imports the values into the config class.
     */
    private void readXMLconfigFile() throws IOException {
        try(FileInputStream in = new FileInputStream(CONFIG_XML)){

            XMLConfig xc;

            xc = JAXB.unmarshal(in, XMLConfig.class);

            xc.getValues(this);

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("could not open " + CONFIG_XML);
            throw e;
        }
    }

    /**
     * writes the current configuration to a xml file.
     *
     * this is done by converting all config data to the format of the XML helper class "XMLConfig"
     * then writing the helper class to the xml file
     */
    private void writeXMLConfigFile(){
        try {
            FileOutputStream out = new FileOutputStream(CONFIG_XML);

            XMLFilters xmlFilters = new XMLFilters();
            XMLProtocol p = new XMLProtocol();
            XMLConvertersString converters = new XMLConvertersString();
            p.prot = protocol;
            Intervals intervals = new Intervals();

            for (Interval interval : intervalMap.values()) {
                intervals.add(interval);
            }

            Map<Sensor, ConvertParameters> pm = converterFactory.getParameterMap();

            for (Sensor sensor : Sensor.values()) {
                ConvertParameters cp = pm.get(sensor);
                if (cp != null) {
                    converters.add(new XMLConverterDataString(sensor, cp.isSigned(), ConvertParser.toString(cp)));
                }
                else {
                    converters.add(new XMLConverterDataString(sensor, true, "csv"));
                }
            }

            for (Sensor sensor : filterFactory.getParameterMap().keySet()) {
                FilterParameters fp = filterFactory.getParameterMap().get(sensor);
                if (fp.getType() == FilterType.FIR){
                    xmlFilters.add(new XMLFilterData(sensor, fp.getAverage(), FilterType.FIR));
                } else
                    xmlFilters.add(new XMLFilterData(sensor, fp.getScale(), FilterType.IIR));

            }

            XMLConfig xc = new XMLConfig();
            xc.offsets = offsets;
            xc.setXmlStringConverters(converters);
            xc.setProtocol(p);
            xc.setIntervals(intervals);
            xc.setFilters(xmlFilters);

            JAXB.marshal(xc,out);

        } catch (FileNotFoundException e1) {
            System.out.println(e1);
        }
    }

    /**
     * sets some default values
     */
    private void setPartDefaults() {

        if (protocol == null){
            final Sensor[] defaultProtocol = new Sensor[]{Sensor.OilPressure, Sensor.AccX, Sensor.AccY, Sensor.AccZ, Sensor.Temp, Sensor.NumValidPackets, Sensor.NumFailedPackets};
            _wirelessProtocol = defaultProtocol;
            protocol = defaultProtocol;
        }

        if (intervalMap == null)
            intervalMap = defaultIntervals();

        if (offsets == null)
            offsets = new float[]{0,0,0};

        if (converterFactory == null){
            converterFactory = new ConverterFactory();

            //to get something in the xml
            for (Sensor sensor : Sensor.values()) {
                converterFactory.getParameterMap().put(sensor,new ConvertParameters(0,0,1, 1, false));
            }
        }
    }

    public Sensor[] getWirelessProtocol(){
        return _wirelessProtocol;
    }

    /**
     * sets the default intervals for some sensors
     * @return interval hash map
     */
    private Map<Sensor,Interval> defaultIntervals(){

        Map<Sensor,Interval> map = new EnumMap<>(Sensor.class);

        map.put(Sensor.AccX,new Interval(Sensor.AccX, DEFAULT_ACCEL_MIN, DEFAULT_ACCEL_MAX));
        map.put(Sensor.AccY, new Interval(Sensor.AccY, DEFAULT_ACCEL_MIN, DEFAULT_ACCEL_MAX));
        map.put(Sensor.AccZ, new Interval(Sensor.AccZ, DEFAULT_ACCEL_MIN, DEFAULT_ACCEL_MAX));

        map.put(Sensor.Temp, new Interval(Sensor.Temp, DEFAULT_TEMP_MIN, DEFAULT_TEMP_MAX));
        map.put(Sensor.OilPressure, new Interval(Sensor.OilPressure, DEFAULT_OIL_PRESSURE_MIN, DEFAULT_OIL_PRESSURE_MAX));

        return map;
    }

    /**
     * singleton getter for this object to make sure that the same configuration is use throe out the program.
     * @return a single (all ways the same) instance of this class
     */
    public synchronized static Config getInstance(){
        if (myself == null)
            myself = new Config();
        return myself;
    }

    public Sensor[] getProtocol() {
        return protocol;
    }

    public ConverterFactory getConverterFactory() {
        return converterFactory;
    }

    /**
     * set a new protocol and notifys the listeners
     * @param protocol a new protocol
     */
    public void setProtocol(Sensor[] protocol) {
        this.protocol = protocol;

        for (ProtocolChangeListener protocolChangeListener : protocolChangeListeners) {
            protocolChangeListener.protocolChange(this.protocol);
        }
    }

    public float getSensorMax(Sensor sensor){
        Interval interval = intervalMap.get(sensor);

        //default is set to 0
        if (interval == null)
            return 0;
        else
            return interval.max;
    }

    public float getSensorMin(Sensor sensor){
        Interval interval = intervalMap.get(sensor);

        //default is set to 0
        if (interval == null)
            return 0;
        else
            return interval.min;
    }

    /**
     * for future development when Data can compensate for the offset
     * @return offset from 0 on the accelerometers
     */
    public float[] getOffset() {
        return offsets;
    }

    public void setOffsetToCurrentValues() {
        offsets[0] = Data.getInstance().getSensorValue(Sensor.AccX);
        offsets[1] = Data.getInstance().getSensorValue(Sensor.AccY);
        offsets[2] = Data.getInstance().getSensorValue(Sensor.AccZ);

        //update the config file
        writeXMLConfigFile();
    }

    /**
     * XML helper class for the configs.
     * This and the data classes with in it is used to read and write the XML file.
     * Note that most functions that is flagged as unused is used by the marshal.
     */
    public static class XMLConfig {
        private XMLProtocol protocol;

        private Intervals intervals;

        private XMLConvertersString xmlStringConverters;
        private XMLFilters filters;

        @XmlAttribute
        public float[] offsets;

        public XMLConfig() {
            protocol = null;
            intervals = null;
            xmlStringConverters = null;
            filters = null;
            offsets = null;
        }

        /**
         * used by the XML marshal
         * @return the intervals
         */
        public Intervals getIntervals() {
            return intervals;
        }

        public void setIntervals(Intervals intervals) {
            this.intervals = intervals;
        }

        /**
         * used by the XML marshal
         * @returns the XMLprotocol
         */
        public XMLProtocol getProtocol() {
            return protocol;
        }

        public void setProtocol(XMLProtocol protoocol) {
            this.protocol = protoocol;
        }

        /**
         * used to map the values from this XML helper class to the real configuration class.
         * @param config the config object to map the values
         * @throws FileNotFoundException
         * @throws IOException
         */
        public void getValues(Config config) throws FileNotFoundException ,IOException {
            config.offsets = offsets;
            config.protocol = protocol.prot;
            config._wirelessProtocol = config.protocol;
            config.intervalMap = intervals.getIntervalMap();

            config.filterFactory = getFilerFactory();
            config.converterFactory = getConvertFactory();
        }

        private ConverterFactory getConvertFactory() throws FileNotFoundException ,IOException{

            ConverterFactory cf = new ConverterFactory();

            for (XMLConverterDataString xmlC : xmlStringConverters.getConverters()) {
                if (xmlC.function.toLowerCase().contains("csv")){
                    cf.getCSVMap().put(xmlC.sensor,loadCSV(xmlC.sensor));
                } else {
                    cf.getParameterMap().put(xmlC.sensor, ConvertParser.getParameters(xmlC));
                }
            }

            return cf;
        }

        /**
         * used by the XML marshall
         * @returns the converter strings. That is the class that have all the XML converters.
         */
        public XMLConvertersString getXmlStringConverters() {
            return xmlStringConverters;
        }

        public void setXmlStringConverters(XMLConvertersString xmlStringConverters) {
            this.xmlStringConverters = xmlStringConverters;
        }

        /**
         * used by the XML marshal
         * @returns the XML class for all the filters.
         */
        public XMLFilters getFilters() {
            return filters;
        }

        public void setFilters(XMLFilters filters) {
            this.filters = filters;
        }

        public FilterFactory getFilerFactory() {
            FilterFactory factory = new FilterFactory();

            for (XMLFilterData filter : filters.getFilters()) {

                FilterParameters fp = new FilterParameters(filter.type, (int) filter.value, filter.value);

                factory.getParameterMap().put(filter.sensor,fp);
            }

            return factory;
        }

        private CSVConverter loadCSV(Sensor sensor) throws IOException, FileNotFoundException {
            final String name = sensor.toString() + ".txt";
            try(FileInputStream inputStream = new FileInputStream(name)){

            int[] x;
            float[] y;

            Scanner sc = new Scanner(inputStream);

            String[] tokens;

            //x
            tokens = sc.nextLine().split(",");
            x = new int[tokens.length];

            for (int i = 0; i < tokens.length; i++) {
                String token = tokens[i];
                x[i] = Integer.parseInt(token);
            }

            //y
            tokens = sc.nextLine().split(",");
            y = new float[tokens.length];

            for (int i = 0; i < tokens.length; i++) {
                String token = tokens[i];
                y[i] = Float.parseFloat(token);
            }

            return new CSVConverter(x,y);

            } catch (FileNotFoundException e){
                System.out.println("could not find the file " + name);
                throw e;
            } catch (IOException e) {
                System.out.println("error while trying to load " + name);
                throw e;
            }
        }
    }

    public FilterFactory getFilterFactory() {
        return filterFactory;
    }

    public void addProtocolChangeListener(ProtocolChangeListener p){
        protocolChangeListeners.add(p);
    }
}
