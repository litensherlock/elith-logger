package common.parsing;

import converters.ConvertParameters;
import dataStructures.XMLHelpers.XMLConverterDataString;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-03-29
 */
public final class ConvertParser {
    private ConvertParser() {
    }

    /**
     * parse the function string and return he parameters for a nonCSV converter.
     * @param dataString contains a function string and a boolean to tell if the value should be interpreted as signed or not.
     * @return parameters for a generic converter
     */
    public static ConvertParameters getParameters(XMLConverterDataString dataString){
        String s = dataString.function;


        String variable = s.substring(s.indexOf('(') + 1,s.indexOf(')'));
        String function;

        float before = 0, mult = 1, div = 1, after = 0;


        final String spaceSparator = " ";
        if (variable.contains(spaceSparator))
            System.err.print("variable " + variable + " in " + s + " illegal character ' '");


        function = s.substring(s.indexOf('=') + 1);

        if (function.contains("("))
        {
            String inSideParantheses = function.substring(function.indexOf('(') + 1, function.indexOf(')'));

            String[] splits = inSideParantheses.split(spaceSparator);
            for (String split : splits) {

                if (!split.contains(variable)){
                    try{
                        before = Float.valueOf(split);

                        //the catches are not identical. if the string is empty we get a NumberFormatException and should thus continue with the next one.
                    } catch (NullPointerException ignored){
                    } catch (NumberFormatException ignored){
                    }
                }

                else {
                    if (split.indexOf("n") == 0)
                        split = split.substring(2, split.length() - 1);
                    else
                        split = split.substring(0,split.indexOf("*"));

                    try{
                        mult = Float.valueOf(split);

                        //the catches are not identical. if the string is empty we get a NumberFormatException and should thus continue with the next one.
                    } catch (NullPointerException ignored){
                    } catch (NumberFormatException ignored){
                    }
                }
            }

            function = 'n' + function.substring(function.indexOf(')') + 1);
        }

        //done with inSideParantheses.
        String[] splits = function.split(spaceSparator);
        for (String split : splits) {

            final char slash = '/';
            if (split.length() > 1 && split.charAt(1) == slash){

                split = split.substring(2, split.length() - 1);

                try{
                    div = Float.valueOf(split);

                    //the catches are not identical. if the string is empty we get a NumberFormatException and should thus continue with the next one.
                } catch (NullPointerException ignored){
                } catch (NumberFormatException ignored){
                }
            } else {
                try{
                    after = Float.valueOf(split);

                    //the catches are not identical. if the string is empty we get a NumberFormatException and should thus continue with the next one.
                } catch (NullPointerException ignored){
                } catch (NumberFormatException ignored){
                }
            }

        }

        return new ConvertParameters(before,after,div,mult,dataString.isSigned);
    }

    /**
     * makes a function string out of convert parameters (from the generic converters)
     * @param cp the convert parameters that is made to a function string
     * @return a function string
     */
    public static String toString(ConvertParameters cp){

        StringBuilder sb = new StringBuilder();
        sb.append("f(n) = ");

        if (cp.getAddBefore() != 0){
            sb.append('(');
            sb.append(cp.getAddBefore()).append(" + ");
            sb.append(cp.getMult()).append("*n)");
        } else {
            if (cp.getMult() != 1) // it should check for exaktly one.
                sb.append(cp.getMult()).append("*n");
            else
                sb.append("n");
        }

        if (cp.getDiv() != 1){ // it should check for exaktly one.
            sb.append('/').append(cp.getDiv());
        }

        if (cp.getAddAfter() != 0)
            sb.append(" + ").append(cp.getAddAfter());

        return sb.toString();
    }
}
