package common;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob lövhall
 * Date: 2013-09-21
 * Time: 14:59
 */

/**
 * Packs a byte array according the low level protocol.
 */
public final class DataPacker {
    private DataPacker(){}

    public static byte[] pack(byte[] bytes) {
        bytes = addLength(bytes);
        bytes = addCheckSum(bytes);
        bytes = escBytes(bytes);
        bytes = addStartStop(bytes);
        return bytes;
    }

    private static byte[] addStartStop(byte[] bytes) {
        byte[] ret = new byte[bytes.length + 2];

        ret[0] = SerialByte.START;
        System.arraycopy(bytes,0,ret,1,bytes.length);
        ret[bytes.length + 1] = SerialByte.END;

        return ret;
    }

    private static byte[] addLength(byte[] bytes) {
        byte [] ret = new byte[bytes.length + 1];
        System.arraycopy(bytes,0,ret,1,bytes.length);
        ret[0] = (byte) bytes.length;
        return ret;
    }

    private static byte[] escBytes(byte[] bytes) {
        int counter = 0;
        int idx = 0;
        byte[] ret;

        for (byte aB1 : bytes)
            if (aB1 == SerialByte.START || aB1 == SerialByte.END || aB1 == SerialByte.ESCAPE)
                counter++;

        //done, no special bytes.
        if(counter == 0)
            return bytes;

        ret = new byte[bytes.length + counter];

        for (byte aB : bytes)
            if (aB == SerialByte.START || aB == SerialByte.END || aB == SerialByte.ESCAPE) {
                ret[idx] = SerialByte.ESCAPE;
                idx++;
                ret[idx] = (byte) (~aB);
                idx++;
            } else {
                ret[idx] = aB;
                idx++;
            }
        return ret;
    }

    private static byte[] addCheckSum(byte[] bytes){
        byte sum = 0;
        byte[] ret;

        for (byte aB : bytes){
            sum += aB;
        }

        ret = new byte[bytes.length + 1];

        System.arraycopy(bytes,0,ret,0,bytes.length);

        ret[bytes.length] = sum;

        return ret;
    }
}
