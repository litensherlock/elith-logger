package common;

import GUI.ERLoggerFrame;
import dataInput.ERin;

import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author jonasbromo
 * edited by Jakob lövhall
 */
public class ERLogger implements ActionListener {

    private ERin in;
    private  Timer timer;

    public ERLogger() {
        int delay=100;

        //create the GUI window.
        final ERLoggerFrame erLoggerFrame = new ERLoggerFrame();

        in = ERin.getInstance();

        timer = new Timer(delay, this);
        timer.start();
     }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /* Set the Nimbus look and feel */

        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if (info.getName().equals("Nimbus")) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException | UnsupportedLookAndFeelException | IllegalAccessException | InstantiationException ex) {
            Logger.getLogger(ERLoggerFrame.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        /* Create and display the form */
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                final ERLogger erLogger = new ERLogger();
            }
        });

    }

    /**
     * Timer action. Read new input.
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        in.readInput();
    }
}
