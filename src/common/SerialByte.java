package common;

/**
 * Created by Jakob Lövhall on 2014-05-04.
 */

/**
 * static class for the constants
 */
public final class SerialByte {
    final public static byte START = (byte) 0xAA;
    final public static byte ESCAPE = (byte) 0xBB;
    final public static byte END = (byte) 0xCC;

    private SerialByte() {
    }
}
