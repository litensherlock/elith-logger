package common;/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import gnu.io.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 *
 * @author jonasbromo
 * edited by Jakob lövhall
 */
public class SerialPortHandler {
    private static final int BAUD_RATE = 57600;
    private static final int PORT_ID = 5000;
    private SerialPort serialPort;
    private OutputStream outStream;
    private InputStream inStream;
    private boolean portOpen = false;
    private static SerialPortHandler mySelf = null;

    public synchronized static SerialPortHandler getInstance(){
        if (mySelf == null)
            mySelf = new SerialPortHandler();

        return mySelf;
    }

    public SerialPortHandler() {
        serialPort = null;
        outStream = null;
        inStream = null;
    }

    public String[] getSerialPortNames() {

        List<String> serialPorts = new ArrayList<>();
        Enumeration<CommPortIdentifier> portList = CommPortIdentifier.getPortIdentifiers();

        while (portList.hasMoreElements()) {

            CommPortIdentifier portId = portList.nextElement();
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                serialPorts.add(portId.getName());
            }
        }

        String[] result = serialPorts.toArray(new String[serialPorts.size()]);
        
        if (result.length == 0) {
            return new String[]{"/dev/ttyACM0"};
        }
        return result;
    }

    public void openSerialPort(String name) {
        try {
            CommPortIdentifier portId = CommPortIdentifier.getPortIdentifier(name);
            serialPort = (SerialPort) portId.open("ER Serial Port", PORT_ID);

            serialPort.setSerialPortParams(
                    BAUD_RATE,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);
            serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_NONE);
            
            System.out.println("Serial port open: " + name);
            
            inStream = serialPort.getInputStream();
            outStream = serialPort.getOutputStream();
            
            portOpen = true;
            
        } catch (NoSuchPortException ex) {
            System.err.println("Failed to open serial port: NoSuchPortException\n" + ex);
        } catch (PortInUseException ex) {
            System.err.println("Failed to open serial port: PortInUseException\n" + ex);
        } catch (UnsupportedCommOperationException ex) {
            System.err.println(ex.getMessage());
        } catch (IOException ex) {
            System.err.println("Failed to get input/output stream\n" + ex);
        }
    }

    public void closeSerialPort(){
        try {
            inStream.close();
            outStream.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.out.println("could not close the connection");
        } catch (NullPointerException ignored){}
    }
    
    /**
     * Get the serial port input stream
     * @return The serial port input stream
     */
    public InputStream getInputStream() {
        return inStream;
    }
 
    /**
     * Get the serial port output stream
     * @return The serial port output stream
     */
    public OutputStream getOutputStream() {
        return outStream;
    }
    
    public boolean isPortOpen()
    {
        return portOpen;
    }
}
