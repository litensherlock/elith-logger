package dataInput;

import common.Config;
import common.ERDataField;
import common.Sensor;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob lövhall
 * Date: 2013-11-17
 * Time: 19:31
 */

//uses a Scanner to handle the parsing from file. delimiters are line break and space. after that sensor names are skipped
public class TextLoader implements Loader{
    private Sensor[] _fileProtocol;
    private Scanner _sc;

    //set up a Scanner to handle the parsing from file.
    public TextLoader(File file) throws FileNotFoundException {
        _fileProtocol = null;

        //the input stream is supposed to be keept open to read from in readPackage() calls.
        _sc =new Scanner(new BufferedInputStream( new FileInputStream(file) ) );

        _sc.useDelimiter("\\r|\\t|\\x20");
    }

    /**
     * @return one time step of sensors if there is any left to read
     */
    @Override
    public ERDataField[] readPackage() {
        ERDataField[] ret;

        //exit if there is no more to read
        if(!_sc.hasNextLine())
            return null;

        //the file protocol are set in the initFile function (inside the if). So if we have a protocol we should just continue to consume lines.
        if(_fileProtocol == null){
            ret = initFile(_sc.nextLine());
        }else
            ret = consumeLine(_sc.nextLine());

        return ret;
    }

    /**
     * Creates new sensor objects from the known protocol.
     * Read the values to the new objects and return them
     */
    private ERDataField[] consumeLine(String s) {
        StringTokenizer tokenizer = new StringTokenizer(s);
        ERDataField[] cur = new ERDataField[_fileProtocol.length];
        int idx = 0;

        for(int i = 0; i < _fileProtocol.length; i++){
            cur[i]=new ERDataField(_fileProtocol[i]);
        }

        while (tokenizer.hasMoreTokens()){
            tokenizer.nextToken();
            int value = Integer.valueOf(tokenizer.nextToken());
            cur[idx].setValue((short)value);
            idx++;
        }
        return cur;
    }

    //reads the first line and adding to the protocol and reading values as it goes.
    private ERDataField[] initFile(String s) {
        int idx = 0;

        StringTokenizer tokenizer = new StringTokenizer(s);

        _fileProtocol=new Sensor[tokenizer.countTokens()/2];

        ERDataField[] cur = new ERDataField[_fileProtocol.length];

        //find the logged sensors
        while (tokenizer.hasMoreElements()){
            _fileProtocol[idx]= Sensor.valueOf(tokenizer.nextToken());

            cur[idx]=new ERDataField(_fileProtocol[idx]);

            int value = Integer.valueOf(tokenizer.nextToken());
            cur[idx].setValue((short)value);
            idx++;
        }

        Config.getInstance().setProtocol(_fileProtocol);

        return cur;
    }
}
