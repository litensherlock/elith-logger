package dataInput;

import common.*;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author jonasbromo
 * edited by Jakob lövhall
 */
public class SerialLoader implements Loader{

    private final static int RX_BUFFER_SIZE = 1000;
    private final static int PACKET_BUFFER_SIZE = 1000;
    private static final int BUFFER_CAPACITY = 4096;
    private static final int ESCAPED_END_BYTE = 0x33;
    private static final int ESCAPED_ESCAPE_BYTE = 0x44;
    private static final int ESCAPED_START_BYTE = 0x55;
    private static final int ERROR_RATE_INTERVAL = 500;
    private static final float PERCENT = 100.0f;

    private byte[] rxBuffer = new byte[RX_BUFFER_SIZE];
    private boolean startByteFound = false;
    private Byte[] packetBuffer = new Byte[PACKET_BUFFER_SIZE];
    private int packetBufferPos = 0;

    private ERDataField[] dataFields = null;

    private int numValidPackets = 0;
    private int numFailedPackets = 0;

    private InputStream inputStream;
    private boolean file = false;

    private Data _data;
    private ArrayList<Byte> bytes;

    public SerialLoader(InputStream inputStream) {
        _data = Data.getInstance();
        this.inputStream = inputStream;
        bytes = new ArrayList<>(BUFFER_CAPACITY);
    }

    public void initWireless(){
        Config config = Config.getInstance();
        config.setProtocol(config.getWirelessProtocol());
        file = false;
    }

    /**
     * reads the version number, the number of sensors and then the sensor types
     * @throws IOException
     */
    public void initFile() throws IOException {
        //the input stream shall remane open so that succesive calls to readPackage() can get some data per call.
        DataInputStream dis = new DataInputStream(inputStream);
        int version;
        int length;

        version = dis.readInt();

        if(version != 1){
            System.out.println("wrong version number " + version + ". only version 1 can be handled");
            return;
        }

        length  = dis.readInt();

        Sensor[] sensors = new Sensor[length];

        for (int i = 0; i < length; i++) {
            sensors[i] = Flags.getSensor(dis.readInt());
        }

        Config.getInstance().setProtocol(sensors);
        file = true;
    }

    @Override
    public ERDataField[] readPackage() {
        if (file){
            newreadSerail();
        } else {
            readSerial(inputStream);
        }
        return dataFields;
    }

    /**
     * Reads a package out of the input stream.
     * Note: this fills the serial buffer and slowly gets out of sync if it is used to read non stored data. e.g the serial stream form the car.
     */
    public void newreadSerail(){
        //the input stream shall remane open so that succesive calls to readPackage() can get some data per call.
        DataInputStream dis = new DataInputStream(inputStream);

        dataFields = null;

        try {
            if (dis.available() == 0)
                return;

            boolean endByteFoundNotFound = true;
            while (dis.available() > 0 && endByteFoundNotFound){

                //check if we are out of space
                if (bytes.size() >= BUFFER_CAPACITY){
                    bytes.clear();
                    System.out.println("buffer overflow. end byte could not be found");
                    return;
                }

                bytes.add(dis.readByte());

                //check if we found the end
                if (bytes.get(bytes.size() - 1) == SerialByte.END){
                    endByteFoundNotFound = false;
                }
            }

            newparsePacket(bytes);
            bytes.clear();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Reads a package out of the input stream.
     * Note: if used on files it will throw away most packages.
     */
    public void readSerial(InputStream inStream) {

        try {
            int bytesAvailable = inStream.available();

            //Don't read more than buffers size (shouldn't occur)
            bytesAvailable = (RX_BUFFER_SIZE < bytesAvailable) ? RX_BUFFER_SIZE : bytesAvailable;

            //Read into the rxBuffer
            int bytesRead = inStream.read(rxBuffer, 0, bytesAvailable);

            for (int i = 0; i < bytesRead; i++) {
                //If start byte, reset packetBuffer
                if (SerialByte.START == rxBuffer[i]) {
                    startByteFound = true;
                    packetBufferPos = 0;
                    packetBuffer[packetBufferPos] = rxBuffer[i];
                    packetBufferPos++;
                } else if (startByteFound) {
                    //Fill the buffer
                    if (PACKET_BUFFER_SIZE > packetBufferPos) {
                        packetBuffer[packetBufferPos] = rxBuffer[i];
                        packetBufferPos++;
                        //End byte found
                        if (SerialByte.END == rxBuffer[i]) {
                            parsePacket(packetBuffer, packetBufferPos);

                            //Reset packetBuffer
                            startByteFound = false;
                            packetBufferPos = 0;
                        }
                    } else {
                        //Packet buffer overrun
                        System.out.println("packetBuffer overrun\n");
                        packetBufferPos = 0;
                        startByteFound = false;
                    }
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            System.err.println("Unable to read serial");
        }
    }

    private void parsePacket(Byte[] rawPacket, int rawLength) {
        //Go through the raw packet and "un-escape" bytes

        if ((rawLength - 3) < _data.getNumberOfSensors()*2) {
            System.out.println("packet length: " + rawLength);
            return;
        }

        assert (rawPacket[0] == SerialByte.START && rawPacket[rawLength - 1] == SerialByte.END);

        UnEscaper unEscaper = new UnEscaper(rawPacket, rawLength).invoke();
        if (unEscaper.isFailed()) return;
        int unEscapedLength = unEscaper.getLength(); //The packet length after un-escaping, not including start/stop-byte
        byte checksum = unEscaper.getChecksum();

        //Subtract the received checksum from the checksum calculation.
        checksum -= rawPacket[unEscapedLength-1];

        //Validate checksum, checksum includes payload length and payload
        if (checksum != rawPacket[unEscapedLength-1]) {
            System.out.println("Invalid checksum: " + checksum + " " + rawPacket[unEscapedLength-1]);
            numFailedPackets++;
            return;
        }

        int payloadLength = rawPacket[0];
        if (payloadLength != _data.getNumberOfSensors()*2 ||
                (unEscapedLength - 2) != _data.getNumberOfSensors()*2) {

            System.out.println("Payload length does not match received packet");
            System.out.println("Payload length: " + payloadLength);
            System.out.println("Packet length: " + unEscapedLength);
            System.out.println("number of sensors: " + _data.getNumberOfSensors());
            return;
        }

        int j = 1; //0 is payload length field

        numValidPackets++;

        //build ER data fields here
        dataFields = new ERDataField[_data.getNumberOfSensors()];

        for (int i = 0; i < _data.getNumberOfSensors(); i++) {
            dataFields[i] = new ERDataField(Data.getInstance().getProtocol()[i]);
            dataFields[i].setValue(rawPacket[j], rawPacket[j+1]);
            j+=2;
        }

        //Print error stats
        int packetsReceived = numValidPackets + numFailedPackets;
        if (0 == (packetsReceived % ERROR_RATE_INTERVAL)) {
            float errorRate = PERCENT * numFailedPackets / packetsReceived;
            System.out.println("Packet error rate: " + errorRate);
        }
    }

    private boolean validEscapedByte(byte b){
        return b == ESCAPED_END_BYTE ||
                b == ESCAPED_ESCAPE_BYTE ||
                b == ESCAPED_START_BYTE;
    }

    private void newparsePacket(List<Byte> rawPacket) {
        //Go through the raw packet and "un-escape" bytes

        if ((rawPacket.size() - 3) < _data.getNumberOfSensors()*2) {
            System.out.println("packet length: " + rawPacket.size());
            return;
        }

        assert (rawPacket.get(0) == SerialByte.START && rawPacket.get(rawPacket.size() - 1) == SerialByte.END);

        final Byte[] tempBytes = rawPacket.toArray(new Byte[rawPacket.size()]);

        //un-escape the bytes
        UnEscaper unEscaper = new UnEscaper(tempBytes,rawPacket.size());
        unEscaper.invoke();
        byte checksum;
        int unEscapedLength = unEscaper.getLength(); // packed length after escaping

        rawPacket = Arrays.asList(tempBytes);

        //Subtract the received checksum from the checksum calculation
        checksum = unEscaper.getChecksum();
        checksum -= rawPacket.get(unEscapedLength - 1);

        //Validate checksum, checksum includes payload length and payload
        if (checksum != rawPacket.get(unEscapedLength-1)) {
            System.out.println("Invalid checksum: " + checksum + " " + rawPacket.get(unEscapedLength-1));
            numFailedPackets++;
            return;
        }

        int payloadLength = rawPacket.get(0);
        if (payloadLength != _data.getNumberOfSensors()*2 || unEscapedLength -2 != _data.getNumberOfSensors()*2) {
            System.out.println("Payload length does not match received packet");
            System.out.println("Payload length: " + payloadLength);
            System.out.println("Packet length: " + unEscapedLength);
            System.out.println("number of sensors: " + _data.getNumberOfSensors());
            return;
        }

        int j = 1; //0 is payload length field

        numValidPackets++;

        //build ER data fields here
        dataFields = new ERDataField[_data.getNumberOfSensors()];

        for (int i = 0; i < _data.getNumberOfSensors(); i++) {
            dataFields[i] = new ERDataField(Data.getInstance().getProtocol()[i]);
            dataFields[i].setValue(rawPacket.get(j), rawPacket.get(j+1));
            j+=2;
        }


        //Print error stats
        int packetsReceived = numValidPackets + numFailedPackets;
        if (0 == (packetsReceived % ERROR_RATE_INTERVAL)) {
            float errorRate = PERCENT * numFailedPackets / packetsReceived;
            System.out.println("Packet error rate: " + errorRate);
        }
    }

    /**
     * used to un-escape the packages.
     */
    private class UnEscaper {
        private boolean myResult;
        private Byte[] rawPacket;
        private int rawLength;
        private byte checksum;
        private int p;

        UnEscaper(Byte[] rawPacket, int rawLength) {
            this.rawPacket = rawPacket;
            this.rawLength = rawLength;
        }

        boolean isFailed() {
            return myResult;
        }

        public byte getChecksum() {
            return checksum;
        }

        public int getLength() {
            return p;
        }

        /**
         * Un-escape the packet in-place
         */
        public UnEscaper invoke() {
            checksum = 0;
            p = 0;

            int i = 0;

            i++; //skipp the start byte;
            while (i < rawLength - 1){
                if (SerialByte.ESCAPE == rawPacket[i]) {

                    i++;//advance to the byte that shall be un-escaped

                    if (validEscapedByte(rawPacket[i])){
                        rawPacket[p] = (byte) (~rawPacket[i]);
                        checksum += rawPacket[p];
                        p++;
                    } else {
                        System.out.println("Invalid escape pattern");
                        numFailedPackets++;
                        myResult = true;
                        return this;
                    }
                } else {
                    rawPacket[p] = rawPacket[i];
                    checksum += rawPacket[p];
                    p++;
                }

                i++;
            }
            myResult = false;
            return this;
        }
    }
}
