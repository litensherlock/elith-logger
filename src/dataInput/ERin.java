package dataInput;

import common.Data;
import common.DataListener;
import common.ERDataField;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob lövhall
 * Date: 2013-11-17
 * Time: 20:28
 */
public class ERin {
    private Loader loader;

    private static ERin myself = null;

    private List<DataListener> listeners;

    /**
     * singleton getter.
     */
    public synchronized static ERin getInstance(){
        if (myself == null)
            myself = new  ERin();

        return myself;
    }

    private ERin() {
        listeners = new ArrayList<>();
        loader = null;
    }

    public void setLoader(Loader in) {
        loader = in;
    }

    public void setInput(File file){
        try {
            if ( file.getName().endsWith(".txt") ){
                loader = new TextLoader(file);
            }else if ( file.getName().endsWith(".bin") ){
                SerialLoader sLoader = new SerialLoader( new FileInputStream(file) );

                sLoader.initFile();

                loader = sLoader;
            }else {
                System.out.println( "format is not supported" );
            }

        } catch (FileNotFoundException e1) {
            System.out.println(e1.getMessage());
            System.out.println("file not found: " + file);
        } catch (IOException e1) {
            System.out.println(e1.getMessage());
            System.out.println("could not initialize the file " + file);
        }
    }

    public void readInput(){
        if(loader == null){ return; }

        ERDataField[] latestData = loader.readPackage();

        Data data = Data.getInstance();

        if(latestData != null){
            data.setNewDataFields(latestData);
            data.setCurrentTime(data.getCurrentTime() + 1);
            notifyListeners();
        }
    }

    public void addListener(DataListener listener){
        listeners.add(listener);
    }

    public void removeListener(DataListener listener){
        listeners.remove(listener);
    }

    private void notifyListeners(){
        for(DataListener listener : listeners)
            listener.dataUpdated(Data.getInstance());
    }

}
