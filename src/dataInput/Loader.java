package dataInput;

import common.ERDataField;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob lövhall
 * Date: 2013-11-17
 * Time: 19:09
 */
public interface Loader {
    public ERDataField[] readPackage();
}
